#!/usr/bin/env python3

import unittest
# import sys,os

# sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from radio_adapters.anytone import Memory


class TestMemory(unittest.TestCase):

    def test_normalize_power(self):

        self.assertEqual(Memory().normalize_power("low"), "Low")
        self.assertEqual(Memory().normalize_power("slow"), "Slow")



if __name__ == '__main__':
    unittest.main()
