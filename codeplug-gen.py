#!/usr/bin/env python3

import sys
import argparse
import logging
import yaml
import importlib

from codeplug_data.model import Model

logging_format = "%(levelname)-5s %(module)s:%(funcName)s %(message)s"
logging.basicConfig(filename="", format=logging_format, level=logging.WARNING)


def create_radio_adapter(config_file):
    logging.debug("create_radio_adapater(%s)" % config_file)

    logging.debug("Reading radio config file: %s" % config_file)
    with open(config_file, 'r') as fh:
        data = yaml.load(fh, Loader=yaml.SafeLoader)
        if 'configuration' not in data:
            logging.fatal("Radio config file is invalid. No top level " +
                          "'configuration' values.")
            sys.exit(1)

    # Read the configuration to find out what radio adapter needs to be loaded
    radio_type = data['configuration']['radio_type']
    adapter_module = importlib.import_module(".%s" % radio_type,
                                             package="radio_adapters")
    adapter = adapter_module.Adapter(data['configuration'])

    return adapter


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Code plug generator')

    parser.add_argument('--debug', '-d', action='store_true',
                        help='Display debugging output')
    parser.add_argument('--verbose', '-v', action='store_true',
                        help='Display verbose output')
    parser.add_argument('--output', '--out', '-o', default='codeplug',
                        metavar='DIR',
                        help='Output director for generated code plug files')
    parser.add_argument('--input', '--in', '-i', default='.', metavar='DIR',
                        help='Directory where code plug data is found')
    parser.add_argument('--sections', '-S', default=[], action='append',
                        help='Generate specific section.')

    # Radio configuration file
    parser.add_argument('radio_config', metavar='RADIO_DEF',
                        help="Radio configuration for code plug")

    args = parser.parse_args()

    if args.verbose:
        logging.getLogger().setLevel(logging.INFO)
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    logging.info("Using %s for code plug generation" % args.radio_config)

    # Read in all the code plug data
    codeplug_data = Model.instance().read_data_files(args)

    # Create the radio adapter
    adapter = create_radio_adapter(args.radio_config)
    adapter.load_codeplug_data(codeplug_data)

    adapter.generate_codeplug(args.output)
