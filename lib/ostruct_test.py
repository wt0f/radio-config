
import pytest

from ostruct import OpenStruct
from pprint import pprint

class TestOpenStuct:

    default_testcase = {
        'abc': 123,
        'def': 456,
        'ghi': 789,
    }

    def test_getitem(self):
        unit = OpenStruct(self.default_testcase)

        assert unit['abc'] == 123
        assert unit['def'] == 456
        assert unit['ghi'] == 789

    def test_getattr(self):
        unit = OpenStruct(self.default_testcase)

        assert unit.abc == 123
        # def is a reserved word and can not be tested here
        assert unit.ghi == 789

    def test_setitem_empty(self):
        unit = OpenStruct({})
        unit['abc'] = 999

        assert unit.abc == 999

    def test_setitem_with_data(self):
        unit = OpenStruct(self.default_testcase)
        unit['xyz'] = 999

        assert unit.xyz == 999

    def test_getitem_nokey(self):
        unit = OpenStruct({})

        with pytest.raises(KeyError):
            unit['abc']

    def test_getattr_nokey(self):
        unit = OpenStruct({})

        with pytest.raises(KeyError):
            unit.abc

    def test_keys(self):
        unit = OpenStruct(self.default_testcase)

        assert unit.keys() == self.default_testcase.keys()

    def test_remove(self):
        unit = OpenStruct(self.default_testcase)

        assert unit.abc == 123
        del(unit['abc'])
        with pytest.raises(KeyError):
            unit['abc']

    def test_value(self):
        unit = OpenStruct(self.default_testcase)

        assert unit.value() == self.default_testcase