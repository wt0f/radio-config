import copy
from collections.abc import MutableMapping

class OpenStruct(MutableMapping):
    def __init__(self, data):
        self._data = copy.deepcopy(data)

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, copy.deepcopy(v, memo))
        return result

    def __getitem__(self, key):
        if type(self._data[key]) in (dict, list):
            return OpenStruct(self._data[key])
        else:
            return self._data[key]

    def __getattr__(self, attr):
        return self.__getitem__(attr)

    def __setitem__(self, key, val):
        if type(val) in (dict, list):
            self._data[key] = OpenStruct(val)
        else:
            self._data[key] = val

    def __delitem__(self, key):
        del(self._data[key])

    def keys(self):
        return self._data.keys()

    def type(self):
        return type(self._data)

    def value(self):
        return self._data

    def __len__(self):
        return self._data.__len__()

    def __iter__(self):
        return self._data.__iter__()

    def __contains__(self, item):
        return self._data.__contains__(item)

    def __reversed__(self):
        return self._data.__reversed__()

    def __str__(self):
        return self._data.__str__()

    def __isinstance__(self, instance):
        return isinstance(self._data, instance)

    def dump(self, indent=0):
        for k in self.keys():
            if isinstance(self[k], OpenStruct):
                print(f"{k}: {self[k].dump(indent=indent+2)}")
            else:
                print(f"{k}: {self[k]}")
