import re


class Base:

    def bool_value(self, val):
        if type(val) == str:
            if re.match(r'on|true|1', val, re.I):
                return 'On'
            else:
                return 'Off'
        else:
            if val:
                return 'On'
            else:
                return 'Off'
