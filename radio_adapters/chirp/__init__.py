"""Anytone radios"""

import sys
import logging
import csv
import re
import pdb

from lib.ostruct import OpenStruct

from pprint import pformat

from .memory import Memory


class Adapter:
    def __init__(self, config):
        self.radio_type = 'chirp'
        self.radio_config = OpenStruct(config)

        self._defaults = { 'bands': [
                {'range': '136-170'},
                {'range': '400-470'},
            ]
        }

    def load_codeplug_data(self, data):
        logging.info("Loading radio settings")
        logging.debug("load_codeplug_data(%s)" % data)

        # merge the default settings data with the codeplug data
        self.codeplug_data = data
        # for setting in self._defaults['settings'].keys():
        #     if self.codeplug_data['settings'][setting]:
        #         # need to define strategy for merging dictionaries
        #         pass
        #     else:
        #         self.codeplug_data['settings'][setting] = self._defaults['settings'][setting]

        # Filter out any stations that can not be used by the radio
        station_to_remove = []
        for station_name in self.codeplug_data.stations.data.keys():
            try:
                rx = self.codeplug_data.stations.data[station_name]['rx']
                tx = self.codeplug_data.stations.data[station_name]['tx']
                if not (self.freq_in_band(rx) and self.freq_in_band(tx)):
                    station_to_remove.append(station_name)
            except KeyError as e:
                logging.critical("{} for station {}".format(e, station_name))
                raise e

        for station_name in station_to_remove:
            logging.info("Removing station {} due to outside of bands".format(station_name))
            self.codeplug_data.stations.remove_station(station_name)
            self.codeplug_data.scanlists.remove_station(station_name)

    def freq_in_band(self, freq):
        bands = self._defaults['bands']
        if 'bands' in self.radio_config.settings:
            bands = self.radio_config.settings.bands

        for band in bands:
            lower, upper = re.split(r'\s*-\s*', band['range'])
            if float(lower) <= float(freq) <= float(upper):
                return True
        return False

    def generate_codeplug(self, dir):
        logging.debug("generate_codeplug(%s)" % dir)

        logging.info("Creating memory file")

        for zone in self.radio_config.zones:
            logging.info("Building out zone {}".format(zone['name']))

            # gather all the channels and add them to the memories and zone
            for query_term in zone['tags']:
                logging.info("Finding stations matching tags {}".format(query_term))


                # gather all the channels and add them to the memories
                for sta in self.codeplug_data.stations.query(query_term):
                    # don't save memory if a dstar or fusion channel
                    if sta['chan_mode'] not in ('dstar', 'fusion', 'dmr'):
                        logging.info("Processing station: %s" % sta['name'])

                        # add station to list of memories
                        mem = Memory()
                        try:
                            logging.debug("Adding frequencies for %s" % sta['name'])
                            name = sta['name']
                            # Allow the radio config setting to specify the
                            # size of the name field (i.e. name: name6)
                            if 'name' in self.radio_config.settings and self.radio_config.settings.name in sta:
                                name = sta[self.radio_config.settings.name]

                            mem.add(name, sta['rx'], sta['tx'])

                            if sta['chan_mode'] == 'fm':
                                logging.debug("Setting station properties for %s" % sta['name'])
                                mem.set_channel_properties("analog", sta['power'], sta['fm_bandwidth'])
                                logging.debug("Setting SQL property: %s" % sta['fm_sql_mode'])
                                mem.set_squelch_properties(sta['fm_sql_mode'])
                                if 'fm_rx_tone' in sta or 'fm_tx_tone' in sta:
                                    rx_tone = 'fm_rx_tone' in sta and sta['fm_rx_tone'] or '88.5'
                                    tx_tone = 'fm_tx_tone' in sta and sta['fm_tx_tone'] or '88.5'
                                    logging.debug("Setting tone property: %s/%s" % (rx_tone, tx_tone))
                                    mem.set_tone_properties(rx_tone, tx_tone)
                                if 'fm_rx_dcs_code' in sta or 'fm_tx_dcs_code' in sta:
                                    rx_code = 'fm_rx_dcs_code' in sta and str(sta['fm_rx_dcs_code']) or '023'
                                    tx_code = 'fm_tx_dcs_code' in sta and str(sta['fm_tx_dcs_code']) or '023'
                                    rx_polarity = 'fm_rx_dcs_polarity' in sta and sta['fm_rx_dcs_polarity'].upper() or 'N'
                                    tx_polarity = 'fm_tx_dcs_polarity' in sta and sta['fm_tx_dcs_polarity'].upper() or 'N'
                                    logging.debug("Setting DCS code: %s/%s" % (rx_code, tx_code))
                                    mem.set_dcs_properties(rx_code, tx_code, rx_polarity, tx_polarity)

                            logging.debug('Adding %s to memories' % sta['name'])
                            mem.commit()

                            # add ordering information to zone if available
                            if 'sequence' in sta:
                                logging.debug("Adding sort sequence: %s with zone %s" % (sta['sequence'], zone['name']))
                                mem.add_sort(sta['sequence'], zone['name'])
                            else:
                                logging.debug("Adding to unsorted frequencies")
                                mem.add_sort(None, name)

                        except Exception as e:
                            import traceback

                            logging.critical("Exception hit for {}".format(sta['name']))
                            logging.critical("{}: {}".format(type(e), e))
                            tb = sys.exc_info()[2]
                            logging.critical(traceback.print_tb(tb))
                            logging.critical("Station data: {}".format(pformat(sta, indent=8)))
                            sys.exit(1)

        logging.info("Creating Channel.CSV")
        mem_settings = 'memory' in self.radio_config.settings.keys() and self.radio_config.settings.memory or dict()
        rows = Memory.render(mem_settings)
        Adapter.write_csv_file("%s/Channel.CSV" % dir, rows)

    @staticmethod
    def write_csv_file(filename, rows):
        with open(filename, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_NONE)
            for row in rows:
                csvwriter.writerow(row)
