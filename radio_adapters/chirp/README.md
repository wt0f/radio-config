Radio Adapter: chirp
====================

Status: alpha

Tested radios
----------------

- Baofeng
    - UV-5R

Supported features
------------------

- Basic frequency, duplex, offset, tone and DCS code

TODO
----

- Skip memory on scan

Notes
-----

Several of the `configuration.settings` are mandatory for the `chirp` radio
adapter. The radio that is being configured may require the `name` setting
to adjust the memory names to the size of the display on the radio. For
example the Baofeng UV-5R can only have memory names that are 6 characters.

Standard configuration
----------------------

```yaml
configuration:
  name: radio-name
  radio_type: chirp
  settings:
    name: name6
    bands:
      - range: 136-174
      - range: 400-480
    memory:
      number: 128
      clear_all: false

  zones:
    ....
```

| Setting       | Required | Notes                                     |
|---------------|----------|-------------------------------------------|
| name          |    no    | Specify which name field to use for all the memories. Allowed values name12, name8, name6 |
| bands.range[] |   yes    | Specify the available frequencies that are supported by the radio |
| memory.number |   yes    | Total number of memories that the radio supports |
| memory.clear_all |  no   | Create empty records to fill out all the memories |
