#!/usr/bin/env python3
import sys
import unittest

from .memory import Memory, Memories
from lib.ostruct import OpenStruct

class TestMemory(unittest.TestCase):

    def tearDown(self):
        Memories.clear()
        Memory._index.clear()
        Memory.sort_index.clear()
        Memory.unsort_index.clear()

    def test_init(self):
        unit = Memory()
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.freq, None)
        self.assertEqual(unit.duplex, '')
        self.assertEqual(unit.offset, 0.0)
        self.assertEqual(unit.chan_type, 'analog')
        self.assertEqual(unit.power, 'high')
        self.assertEqual(unit.bandwidth, 'wide')
        self.assertEqual(unit.rx_tone, '88.5')
        self.assertEqual(unit.tx_tone, '88.5')
        self.assertEqual(unit.dcs_code, '023')
        self.assertEqual(unit.dcs_polarity, 'NN')
        self.assertEqual(unit.mode, None)
        self.assertEqual(unit.step, '5.00')
        self.assertEqual(unit.skip, None)
        self.assertEqual(unit.urcall, None)
        self.assertEqual(unit.rpt1_call, None)
        self.assertEqual(unit.rpt2_call, None)
        self.assertEqual(unit.dvcode, None)

    def test_add(self):
        unit = Memory()
        unit.add('Local', '146.52', '147.52')
        self.assertEqual(unit.name, 'Local')
        self.assertEqual(unit.freq, '146.52')
        self.assertEqual(unit.offset, '1.0')
        self.assertEqual(unit.duplex, '+')

        unit.add('Remote', 443.450, 442.125)
        self.assertEqual(unit.name, 'Remote')
        self.assertEqual(unit.freq, '443.45')
        self.assertEqual(unit.offset, '1.325')
        self.assertEqual(unit.duplex, '-')

    def test_set_channel_properties(self):
        unit = Memory()
        unit.set_channel_properties('digital', 'med', 'narrow')
        self.assertEqual(unit.chan_type, 'digital')
        self.assertEqual(unit.power, 'med')
        self.assertEqual(unit.bandwidth, 'narrow')

    def test_squelch_properties(self):
        unit = Memory()
        unit.set_squelch_properties('none')
        self.assertEqual(unit.mode, 'none')

    def test_set_tone_properties(self):
        unit = Memory()
        unit.set_tone_properties('103.5', '123.0')
        self.assertEqual(unit.rx_tone, '103.5')
        self.assertEqual(unit.tx_tone, '123.0')

        unit.set_tone_properties(71.9, 203.5)
        self.assertEqual(unit.rx_tone, '71.9')
        self.assertEqual(unit.tx_tone, '203.5')

        unit.set_tone_properties(123, 123)
        self.assertEqual(unit.rx_tone, '123.0')
        self.assertEqual(unit.tx_tone, '123.0')

        unit.set_tone_properties(None, None)
        self.assertEqual(unit.rx_tone, '88.5')
        self.assertEqual(unit.tx_tone, '88.5')

        unit.set_tone_properties('Off', 'Off')
        self.assertEqual(unit.rx_tone, '88.5')
        self.assertEqual(unit.tx_tone, '88.5')

    def test_set_dcs_properties(self):
        unit = Memory()
        unit.set_dcs_properties('073', '021', 'N', 'R')
        self.assertEqual(unit.dcs_code, '021')
        self.assertEqual(unit.dcs_polarity, 'NR')

        unit.set_dcs_properties(23, 412, 'N', 'N')
        self.assertEqual(unit.dcs_code, '412')
        self.assertEqual(unit.dcs_polarity, 'NN')

        unit.set_dcs_properties('114', 6, 'R', 'R')
        self.assertEqual(unit.dcs_code, '006')
        self.assertEqual(unit.dcs_polarity, 'RR')

        unit.set_dcs_properties(None, None, 'N', 'N')
        self.assertEqual(unit.dcs_code, '023')

        unit.set_dcs_properties('Off', 'Off', 'N', 'N')
        self.assertEqual(unit.dcs_code, '023')

    def test_normalize_power(self):
        self.assertEqual(Memory.normalize_power('slow'), 'Low')
        self.assertEqual(Memory.normalize_power('low'), 'Low')
        self.assertEqual(Memory.normalize_power('medlo'), 'Med')
        self.assertEqual(Memory.normalize_power('med'), 'Med')
        self.assertEqual(Memory.normalize_power('medhi'), 'Med')
        self.assertEqual(Memory.normalize_power('high'), 'High')
        self.assertEqual(Memory.normalize_power('shigh'), 'Turbo')
        self.assertEqual(Memory.normalize_power('turbo'), 'Turbo')

    def test_normalize_sql_mode(self):
        self.assertEqual(Memory.normalize_sql_mode('tone'), 'Tone')
        self.assertEqual(Memory.normalize_sql_mode('tsql'), 'TSQL')
        self.assertEqual(Memory.normalize_sql_mode('dcs'), 'DTCS')
        self.assertEqual(Memory.normalize_sql_mode('off'), '')
        self.assertEqual(Memory.normalize_sql_mode('none'), '')
        self.assertEqual(Memory.normalize_sql_mode(None), '')

    def test_normalize_frequency(self):
        self.assertEqual(Memory.normalize_frequency('123'), '123.000000')
        self.assertEqual(Memory.normalize_frequency(123), '123.000000')
        self.assertEqual(Memory.normalize_frequency('123.3'), '123.300000')
        self.assertEqual(Memory.normalize_frequency('123.300'), '123.300000')
        self.assertEqual(Memory.normalize_frequency('0.250'), '0.250000')
        self.assertEqual(Memory.normalize_frequency(0.3905), '0.390500')

    def test_normalize_offset(self):
        self.assertEqual(Memory.normalize_offset('0.0'), '0.000000')
        self.assertEqual(Memory.normalize_offset('1.0'), '1.000000')
        self.assertEqual(Memory.normalize_offset('0.05'), '0.050000')
        self.assertEqual(Memory.normalize_offset('13.7'), '13.700000')

    def test_normalize_chan_type(self):
        self.assertEqual(Memory.normalize_chan_type('analog', 'wide'), 'FM')
        self.assertEqual(Memory.normalize_chan_type('analog', 'narrow'), 'NFM')


    def test_render(self):
        unit = Memory()
        unit.add('ICST 1', 146.56, 146.56)
        unit.set_channel_properties('analog', 'high', 'wide')
        unit.set_tone_properties(71.9, 71.9)
        unit.set_squelch_properties('tsql')
        unit.commit()
        unit.add_sort('a1', 'ICST 1')

        unit = Memory()
        unit.add('70 CM Calling', 446.000, 446.000)
        unit.set_channel_properties('analog', 'med', 'narrow')
        unit.set_squelch_properties('off')
        unit.commit()
        unit.add_sort('b2', '70 CM Calling')

        unit = Memory()
        unit.add('aRepeater', '147.54', '146.54')
        unit.set_channel_properties('analog', 'high', 'wide')
        unit.set_dcs_properties('073', '056', 'R', 'N')
        unit.set_squelch_properties('dcs')
        unit.commit()
        unit.add_sort('a0', 'aRepeater')

        output = Memory.render({})
        print(output, file=sys.stderr)
        self.assertEqual(output[0], Memory.csv_labels)

        self.assertEqual(output[1][0], 0)
        self.assertEqual(output[1][1], 'aRepeater')
        self.assertEqual(output[1][2], Memory.normalize_frequency(147.54))
        self.assertEqual(output[1][3], '-')
        self.assertEqual(output[1][4], Memory.normalize_offset('1.0'))
        self.assertEqual(output[1][5], 'DTCS')
        self.assertEqual(output[1][6], '88.5')
        self.assertEqual(output[1][7], '88.5')
        self.assertEqual(output[1][8], '056')
        self.assertEqual(output[1][9], 'RN')
        self.assertEqual(output[1][10], Memory.normalize_chan_type('analog', 'wide'))

        self.assertEqual(output[2][0], 1)
        self.assertEqual(output[2][1], 'ICST 1')
        self.assertEqual(output[2][2], Memory.normalize_frequency(146.56))
        self.assertEqual(output[2][3], '')
        self.assertEqual(output[2][4], Memory.normalize_offset('0'))
        self.assertEqual(output[2][5], 'TSQL')
        self.assertEqual(output[2][6], '71.9')
        self.assertEqual(output[2][7], '71.9')
        self.assertEqual(output[2][8], '023')
        self.assertEqual(output[2][9], 'NN')
        self.assertEqual(output[2][10], Memory.normalize_chan_type('analog', 'wide'))

        self.assertEqual(output[3][0], 2)
        self.assertEqual(output[3][1], '70 CM Calling')
        self.assertEqual(output[3][2], Memory.normalize_frequency(446.000))
        self.assertEqual(output[3][3], '')
        self.assertEqual(output[3][4], Memory.normalize_offset('0'))
        self.assertEqual(output[3][5], '')
        self.assertEqual(output[3][6], '88.5')
        self.assertEqual(output[3][7], '88.5')
        self.assertEqual(output[3][8], '023')
        self.assertEqual(output[3][9], 'NN')
        self.assertEqual(output[3][10], Memory.normalize_chan_type('analog', 'narrow'))

if __name__ == '__main__':
    unittest.main()
