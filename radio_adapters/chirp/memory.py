import logging
import pdb
import sys

from .base import Base
from lib.ostruct import OpenStruct

Memories = dict()

# Anytone CPS columns: https://www.repeaterbook.com/wiki/doku.php?id=anytone_d878uv_cps

class Memory(Base):
    csv_labels = ("Location",
                  "Name",
                  "Frequency",
                  "Duplex",
                  "Offset",
                  "Tone",
                  "rToneFreq",
                  "cToneFreq",
                  "DtcsCode",
                  "DtcsPolarity",
                  "Mode",
                  "Tstep",
                  "Skip",
                  "Comment",
                  "URCALL",
                  "RPT1CALL",
                  "RPT2CALL",
                  "DVCODE")

    _index = []
    sort_index = dict()
    unsort_index = list()

    def __init__(self):
        self.name                  = None
        self.freq                  = None
        self.duplex                = ''
        self.offset                = 0.0
        self.chan_type             = "analog"  # digital
        self.power                 = "high"       # low, Mid, High, Turbo
        self.bandwidth             = "wide"       # 12.5K
        self.rx_tone               = "88.5"       # CTCSS code
        self.tx_tone               = "88.5"       # CTCSS code
        self.dcs_code              = "023"       # DCS code
        self.dcs_polarity          = 'NN'
        self.mode                  = None
        self.step                  = '5.00'
        self.skip                  = None
        self.urcall                = None
        self.rpt1_call             = None
        self.rpt2_call             = None
        self.dvcode                = None

    def add(self, name, rx, tx):
        logging.debug("Memory.add({}, {}, {})".format(name, rx, tx))
        self.name = name
        self.freq = self.rx = str(rx)
        if (rx != tx):
            offset = round(float(rx) - float(tx), 5)
            self.offset = str(offset)
            if offset < 0:
                # positive offset
                self.duplex = '+'
                self.offset = str(abs(offset))
            else:
                # negative offset
                self.duplex = '-'


    def add_sort(self, key, zone_name):
        sort_key = key

        if key == None:
            Memory.unsort_index.append(self.name)
            return

        if type(key) == dict:
            if zone_name in key:
                sort_key = key[zone_name]
            else:
                Memory.unsort_index.append(self.name)
                return

        if sort_key in Memory.sort_index:
            Memory.sort_index[sort_key].append(self.name)
        else:
            Memory.sort_index[sort_key] = [self.name]

    def set_channel_properties(self, chan_type, power, bw):
        self.chan_type = chan_type
        self.power = power
        self.bandwidth = bw

    def set_squelch_properties(self, mode):
        logging.debug(f'({mode})')
        self.mode = mode

    def set_tone_properties(self, rx_tone, tx_tone):
        if (rx_tone is None) or (rx_tone == 'Off'):
          self.rx_tone = '88.5'
        else:
          self.rx_tone = "%.1f" % float(rx_tone)

        if (tx_tone is None) or (tx_tone == 'Off'):
          self.tx_tone = '88.5'
        else:
          self.tx_tone = "%.1f" % float(tx_tone)

    def set_dcs_properties(self, rx_code, tx_code, rx_polarity, tx_polarity):
        if (rx_code is None) or (rx_code == 'Off'):
          self.dcs_code = '023'
        else:
          self.dcs_code = "%03d" % int(rx_code)

        if (tx_code is None) or (tx_code == 'Off'):
          self.dcs_code = '023'
        else:
          self.dcs_code = "%03d" % int(tx_code)

        self.dcs_polarity = f"{rx_polarity}{tx_polarity}"

    def set_scan_list(self, name):
        self.scan_list = name

    def commit(self):
        if self.name not in Memory._index:
            Memory._index.append(self.name)

            record = {'name': self.name,
                      'freq': self.freq,
                      'duplex': self.duplex,
                      'offset': self.offset,
                      'chan_type': self.chan_type,
                      'power': self.power,
                      'bandwidth': self.bandwidth,
                      'rx_tone': self.rx_tone,
                      'tx_tone': self.tx_tone,
                      'dcs_code': self.dcs_code,
                      'dcs_polarity': self.dcs_polarity,
                      'mode': self.mode,
                      'step': self.step,
                      'skip': self.skip,
                      'urcall': self.urcall,
                      'rpt1_call': self.rpt1_call,
                      'rpt2_call': self.rpt2_call,
                      'dvcode': self.dvcode,}
            Memories[self.name] = OpenStruct(record)

    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return Memory.name(args)
        return method


    @staticmethod
    def findByName(name):
        if name in Memories.keys():
            return Memories[name]

        return None

    @staticmethod
    def normalize_power(power):
        if power == "slow":
            power = "low"
        elif power == "medlo":
            power = "med"
        elif power == "medhi":
            power = "med"
        elif power == 'shigh':
            power = "turbo"
        return power.title()

    @staticmethod
    def normalize_sql_mode(mode):
        sql_map = {'tone': 'Tone', 'tsql': 'TSQL', 'dcs': 'DTCS',
                   'off': '', 'none': ''}
        if mode in sql_map.keys():
            logging.debug(f'({mode}) = {sql_map[mode]}')
            return sql_map[mode]
        elif not mode:
            logging.debug(f'({mode}) = ""')
            return ''
        else:
            raise RuntimeError(f'Invalid squelch mode: {mode}')

    @staticmethod
    def normalize_frequency(freq):
        return "%.6f" % float(freq)

    @staticmethod
    def normalize_offset(freq):
        return "%0.6f" % float(freq)

    @staticmethod
    def normalize_chan_type(mode, bw):
        if mode == 'analog' and bw == 'wide':
            return 'FM'
        elif mode == 'analog' and bw == 'narrow':
            return 'NFM'
        else:
            return None

    @staticmethod
    def render(mem_config):
        count = 0
        output = [ Memory.csv_labels]

        seq_keys = list(Memory.sort_index.keys())
        seq_keys.sort()
        logging.info("Generating memories for stations with sort keys")
        for seq in seq_keys:
            logging.debug("Generating memories with sort key: %s" % seq)
            for name in Memory.sort_index[seq]:
                logging.debug("Writing memory for %s" % name)
                entry = Memory.findByName(name)

                try:
                    output.append((count,
                                   entry.name,
                                   Memory.normalize_frequency(entry.freq),
                                   entry.duplex,
                                   Memory.normalize_offset(entry.offset),
                                   Memory.normalize_sql_mode(entry.mode),
                                   entry.rx_tone,
                                   entry.tx_tone,
                                   entry.dcs_code,
                                   entry.dcs_polarity,
                                   Memory.normalize_chan_type(entry.chan_type, entry.bandwidth),
                                   entry.step,
                                   entry.skip,
                                   '',
                                   entry.urcall,
                                   entry.rpt1_call,
                                   entry.rpt2_call,
                                   entry.dvcode,
                        ))
                except Exception as e:
                    logging.critical(f'{e}')
                    logging.critical(f'{entry.dump()=}')
                    sys.exit(1)
                count += 1

        # now add any memories that don't have a sort index
        logging.info('Generating memories for stations without sort keys')
        for name in Memory.unsort_index:
            logging.debug("Writing memory for %s" % name)
            entry = Memory.findByName(name)

            try:
                output.append((count,
                               entry.name,
                               Memory.normalize_frequency(entry.freq),
                               entry.duplex,
                               Memory.normalize_offset(entry.offset),
                               Memory.normalize_sql_mode(entry.mode),
                               entry.rx_tone,
                               entry.tx_tone,
                               entry.dcs_code,
                               entry.dcs_polarity,
                               Memory.normalize_chan_type(entry.chan_type, entry.bandwidth),
                               entry.step,
                               entry.skip,
                               '',
                               entry.urcall,
                               entry.rpt1_call,
                               entry.rpt2_call,
                               entry.dvcode,
                    ))
            except Exception as e:
                logging.critical(f'{e}')
                logging.critical(f'{entry.dump()=}')
                sys.exit(1)
            count += 1

        # Chirp does not delete all memories before import.
        # we will write empty data to the rest of memories.
        if 'clear_all' in mem_config:
            for num in range(count, mem_config.number):
                output.append((count,
                               '',
                               Memory.normalize_frequency('0.0'),
                               '',
                               Memory.normalize_offset('0.0'),
                               '',
                               '88.5',
                               '88.5',
                               '0',
                               'NN',
                               Memory.normalize_chan_type('analog', 'wide'),
                               '5.00',
                               '',
                               '',
                               '',
                               '',
                               '',
                               '', ))
                count += 1

        return output
