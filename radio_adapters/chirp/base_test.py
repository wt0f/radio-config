#!/usr/bin/env python3
import sys
import unittest

from .base import Base

class TestBase(unittest.TestCase):

    def test_bool_value(self):
        unit = Base()
        self.assertEqual(unit.bool_value('on'), 'On')
        self.assertEqual(unit.bool_value('On'), 'On')
        self.assertEqual(unit.bool_value('ON'), 'On')
        self.assertEqual(unit.bool_value('true'), 'On')
        self.assertEqual(unit.bool_value('True'), 'On')
        self.assertEqual(unit.bool_value('TRUE'), 'On')
        self.assertEqual(unit.bool_value(True), 'On')

        self.assertEqual(unit.bool_value('off'), 'Off')
        self.assertEqual(unit.bool_value('Off'), 'Off')
        self.assertEqual(unit.bool_value('OFF'), 'Off')
        self.assertEqual(unit.bool_value('false'), 'Off')
        self.assertEqual(unit.bool_value('False'), 'Off')
        self.assertEqual(unit.bool_value('FALSE'), 'Off')
        self.assertEqual(unit.bool_value('Anyvalue'), 'Off')
        self.assertEqual(unit.bool_value(False), 'Off')

        self.assertEqual(unit.bool_value(1), 'On')
        self.assertEqual(unit.bool_value(0), 'Off')
        self.assertEqual(unit.bool_value('1'), 'On')
        self.assertEqual(unit.bool_value('0'), 'Off')


if __name__ == '__main__':
    unittest.main()
