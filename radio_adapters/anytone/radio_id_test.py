#!/usr/bin/env python3
import sys
import unittest

from .radio_id import RadioID, RadioIDs, csv_labels
from lib.ostruct import OpenStruct

class TestRadioID(unittest.TestCase):

    def tearDown(self):
        RadioIDs.clear()

    def test_init_obj(self):
        unit = RadioID()
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.id, None)

    def test_add(self):
        unit = RadioID()
        unit.add('some_name', '4434')
        self.assertEqual(unit.name, 'some_name')
        self.assertEqual(unit.id, '4434')

    def test_findByName(self):
        unit = RadioID()
        unit.add('id:1', '123')
        unit = RadioID()
        unit.add('id:2', '321')
        unit = RadioID()
        unit.add('id:3', '444')
        unit = RadioID()
        unit.add('id:4', '88392')

        self.assertIsInstance(RadioID.findByName('id:1'), OpenStruct)
        self.assertEqual(RadioID.findByName('id:2').name, 'id:2')
        self.assertEqual(RadioID.findByName('id:4').id, '88392')


    def test_render_single_id(self):
        unit = RadioID()
        unit.add('id-1', '9921')

        output = RadioID().render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, '9921', 'id-1'))

    def test_render_multi_id(self):
        unit = RadioID()
        unit.add('id#1', '12345')
        unit.add('id-2', '9999')
        unit.add('id*3', '04031')

        output = RadioID().render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, '12345', 'id#1'))
        self.assertTrue(output[2] == (2, '9999', 'id-2'))
        self.assertTrue(output[3] == (3, '04031', 'id*3'))


if __name__ == '__main__':
    unittest.main()
