#!/usr/bin/env python3
import sys
import unittest

from .repeater_offset import RepeaterOffset, csv_labels, band_offsets
from lib.ostruct import OpenStruct

class TestRepeaterOffset(unittest.TestCase):

    def tearDown(self):
        unit = None

    def test_init_obj(self):
        unit = RepeaterOffset()
        self.assertEqual(unit.data, band_offsets)

    def test_add(self):
        unit = RepeaterOffset()
        unit.add('2M', '1.0')
        self.assertEqual(unit.data['2M'], 1.0)

    def test_render_defaults(self):
        unit = RepeaterOffset()

        output = unit.render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, "600.00 KHz"))
        self.assertTrue(output[2] == (2, "5.00000 MHz"))
        self.assertTrue(output[3] == (3, "1.60000 MHz"))

    def test_render(self):
        unit = RepeaterOffset()
        unit.add('2M', 0.8)
        unit.add('1.25M', '3.0')
        unit.add('70CM', '1.8')

        output = unit.render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, "800.00 KHz"))
        self.assertTrue(output[2] == (2, "1.80000 MHz"))
        self.assertTrue(output[3] == (3, "3.00000 MHz"))

if __name__ == '__main__':
    unittest.main()
