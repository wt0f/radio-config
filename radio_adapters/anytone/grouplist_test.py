#!/usr/bin/env python3
import sys
import unittest

from .grouplist import GroupList, GroupLists, csv_labels, _index

class TestGroupList(unittest.TestCase):

    def tearDown(self):
        GroupLists.clear()
        _index.clear()

    def test_init_obj(self):
        unit = GroupList()
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.members, [])
        self.assertEqual(unit.members_tg, [])

    def test_add(self):
        unit = GroupList()
        unit.add('some_name')
        self.assertEqual(unit.name, 'some_name')

    def test_add_member(self):
        unit = GroupList()
        unit.add_member('tg_01', '555')
        self.assertTrue('tg_01' in unit.members)
        self.assertTrue('555' in unit.members_tg)

    def test_render_single_member(self):
        unit = GroupList()
        unit.add('single talkgroup')
        unit.add_member('tg01', '123')
        unit.commit()

        output = GroupList.render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, 'single talkgroup', 'tg01', '123'))

    def test_render_multi_member(self):
        unit = GroupList()
        unit.add('multi talkgroup')
        unit.add_member('tg01', '123')
        unit.add_member('tg02', '456')
        unit.commit()

        output = GroupList.render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, 'multi talkgroup', 'tg01|tg02', '123|456'))

    def test_render_multi_commit(self):
        unit = GroupList()
        unit.add('first talkgroup')
        unit.add_member('tg01', '123')
        unit.commit()

        unit = GroupList()
        unit.add('second talkgroup')
        unit.add_member('tg02', '456')
        unit.commit()

        unit = GroupList()
        unit.add('both talkgroups')
        unit.add_member('tg01', '123')
        unit.add_member('tg02', '456')
        unit.commit()

        output = GroupList.render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, 'first talkgroup', 'tg01', '123'))
        self.assertTrue(output[2] == (2, 'second talkgroup', 'tg02', '456'))
        self.assertTrue(output[3] == (3, 'both talkgroups', 'tg01|tg02', '123|456'))

if __name__ == '__main__':
    unittest.main()
