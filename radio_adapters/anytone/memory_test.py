#!/usr/bin/env python3
import sys
import unittest

from .memory import Memory, Memories
from lib.ostruct import OpenStruct

class TestMemory(unittest.TestCase):

    config = OpenStruct({
        'name': 'Anytone test harness',
        'radio_type': 'anytone',
        'model': 'at578',
        'fw_version': '1.0',
        'settings': {
            'bands': [
                { 'range': '136-174' },
                { 'range': '400-480' },
            ]
        }
    })

    def tearDown(self):
        Memories.clear()
        Memory._index.clear()
        Memory._headers.clear()
        Memory._attrs.clear()

    def test_init(self):
        unit = Memory(self.config)
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.rx, None)
        self.assertEqual(unit.tx, None)
        self.assertEqual(unit.chan_type, 'analog')
        self.assertEqual(unit.power, 'low')
        self.assertEqual(unit.bandwidth, 'wide')
        self.assertEqual(unit.rx_tone, 'Off')
        self.assertEqual(unit.tx_tone, 'Off')
        self.assertEqual(unit.rx_dcs_code, 'Off')
        self.assertEqual(unit.tx_dcs_code, 'Off')
        self.assertEqual(unit.rx_dcs_polarity, 'N')
        self.assertEqual(unit.tx_dcs_polarity, 'N')
        self.assertEqual(unit.contact, None)
        self.assertEqual(unit.contact_type, None)
        self.assertEqual(unit.contact_tg, None)
        self.assertEqual(unit.radio_id, None)
        self.assertEqual(unit.tx_permit, 'Off')
        self.assertEqual(unit.squelch_mode, 'Carrier')
        self.assertEqual(unit.opt_signal, 'Off')
        self.assertEqual(unit.dtmf_id, '1')
        self.assertEqual(unit.tone2_id, '1')
        self.assertEqual(unit.tone5_id, '1')
        self.assertEqual(unit.ptt_id, 'Off')
        self.assertEqual(unit.color_code, '1')
        self.assertEqual(unit.slot, '1')
        self.assertEqual(unit.scan_list, 'None')
        self.assertEqual(unit.rx_group_list, 'None')
        self.assertEqual(unit.ptt_prohibit, 'Off')
        self.assertEqual(unit.reverse, 'Off')
        self.assertEqual(unit.tdma, 'Off')
        self.assertEqual(unit.tdma_adaptive, 'Off')
        self.assertEqual(unit.aes_encryption, 'Normal Encryption')
        self.assertEqual(unit.encryption, 'Off')
        self.assertEqual(unit.call_confirm, 'Off')
        self.assertEqual(unit.talk_around, 'Off')
        self.assertEqual(unit.work_alone, 'Off')
        self.assertEqual(unit.custom_tone, '251.1')
        self.assertEqual(unit.tone2_decode, '1')
        self.assertEqual(unit.ranging, 'Off')
        self.assertEqual(unit.simplex, 'Off')
        self.assertEqual(unit.aprs_rx, 'Off')
        self.assertEqual(unit.aprs_analog_ptt_mode, 'Off')
        self.assertEqual(unit.aprs_digital_ptt_mode, 'Off')
        self.assertEqual(unit.aprs_report_type, 'Off')
        self.assertEqual(unit.aprs_digital_channel, '1')
        self.assertEqual(unit.freq_correction, '0')
        self.assertEqual(unit.sms_confirmation, 'Off')
        self.assertEqual(unit.roaming_exclude, '0')
        self.assertEqual(unit.dmr_mode, '0')
        self.assertEqual(unit.disable_dataack, '0')
        self.assertEqual(unit.r5tone_bot, '0')
        self.assertEqual(unit.r5tone_eot, '0')
        self.assertEqual(unit.auto_scan, '0')
        self.assertEqual(unit.aprs_mute, '0')
        self.assertEqual(unit.talker_alias, '0')
        self.assertEqual(unit.aprs_tx_path, '0')

    def test_add(self):
        unit = Memory(self.config)
        unit.add('Local', '146.52', '147.52')
        self.assertEqual(unit.name, 'Local')
        self.assertEqual(unit.rx, '146.52')
        self.assertEqual(unit.tx, '147.52')

        unit.add('Remote', 443.450, 442.125)
        self.assertEqual(unit.name, 'Remote')
        self.assertEqual(unit.rx, '443.45')
        self.assertEqual(unit.tx, '442.125')

    def test_tx_inhibit(self):
        unit = Memory(self.config)
        for val in (True, 'On', 'on', 'ON', 'true', 'True', 'TRUE', '1', 1):
            unit.set_tx_inhibit(val)
            print(val, file=sys.stderr)
            print(type(val), file=sys.stderr)
            self.assertEqual(unit.ptt_prohibit, 'On')

        for val in (False, 'off', 'Off', 'OFF', 'false', 'False', 'FALSE', '0', 0):
            unit.set_tx_inhibit(val)
            self.assertEqual(unit.ptt_prohibit, 'Off')

    def test_set_channel_properties(self):
        unit = Memory(self.config)
        unit.set_channel_properties('digital', 'med', 'narrow')
        self.assertEqual(unit.chan_type, 'digital')
        self.assertEqual(unit.power, 'med')
        self.assertEqual(unit.bandwidth, 'narrow')

    def test_squelch_properties(self):
        unit = Memory(self.config)
        unit.set_squelch_properties('none')
        self.assertEqual(unit.squelch_mode, 'none')

    def test_set_tone_properties(self):
        unit = Memory(self.config)
        unit.set_tone_properties('103.5', '123.0')
        self.assertEqual(unit.rx_tone, '103.5')
        self.assertEqual(unit.tx_tone, '123.0')

        unit.set_tone_properties(71.9, 203.5)
        self.assertEqual(unit.rx_tone, '71.9')
        self.assertEqual(unit.tx_tone, '203.5')

        unit.set_tone_properties(123, 123)
        self.assertEqual(unit.rx_tone, '123.0')
        self.assertEqual(unit.tx_tone, '123.0')

        unit.set_tone_properties(None, None)
        self.assertEqual(unit.rx_tone, 'Off')
        self.assertEqual(unit.tx_tone, 'Off')

        unit.set_tone_properties('Off', 'Off')
        self.assertEqual(unit.rx_tone, 'Off')
        self.assertEqual(unit.tx_tone, 'Off')

    def test_set_dcs_properties(self):
        unit = Memory(self.config)
        unit.set_dcs_properties('073', '021', 'N', 'R')
        self.assertEqual(unit.rx_tone, 'D073N')
        self.assertEqual(unit.tx_tone, 'D021R')

        unit.set_dcs_properties(23, 412, 'N', 'N')
        self.assertEqual(unit.rx_tone, 'D023N')
        self.assertEqual(unit.tx_tone, 'D412N')

        unit.set_dcs_properties('114', 6, 'R', 'R')
        self.assertEqual(unit.rx_tone, 'D114R')
        self.assertEqual(unit.tx_tone, 'D006R')

        unit.set_dcs_properties(None, None, 'N', 'N')
        self.assertEqual(unit.rx_tone, 'Off')
        self.assertEqual(unit.tx_tone, 'Off')

        unit.set_dcs_properties('Off', 'Off', 'N', 'N')
        self.assertEqual(unit.rx_tone, 'Off')
        self.assertEqual(unit.tx_tone, 'Off')

    def test_set_contact_info(self):
        unit = Memory(self.config)
        unit.set_contact_info('Fred', 'private', '56128')
        self.assertEqual(unit.contact, 'Fred')
        self.assertEqual(unit.contact_type, 'private')
        self.assertEqual(unit.contact_tg, '56128')

    def test_set_dmr_properties(self):
        unit = Memory(self.config)
        unit.set_dmr_properties('repeater', '2', '4', '1234')
        self.assertEqual(unit.dmr_mode, 'repeater')
        self.assertEqual(unit.slot, '2')
        self.assertEqual(unit.color_code, '4')
        self.assertEqual(unit.radio_id, '1234')

        unit.set_dmr_properties('dual_slot', 2, 6, 1234)
        self.assertEqual(unit.dmr_mode, 'dual_slot')
        self.assertEqual(unit.slot, '2')
        self.assertEqual(unit.color_code, '6')
        self.assertEqual(unit.radio_id, '1234')

    def test_set_dmr_tx_allow(self):
        unit = Memory(self.config)
        unit.set_dmr_tx_allow('on')
        self.assertEqual(unit.tx_permit, 'on')

    def test_set_analog_aprs(self):
        unit = Memory(self.config)
        unit.set_analog_aprs('144.39', 'eot')
        self.assertEqual(unit.aprs_report_type, 'Analog')
        self.assertEqual(unit.aprs_rx, '144.39')
        self.assertEqual(unit.aprs_analog_ptt_mode, 'eot')

        unit.set_analog_aprs(144.390, 'bot')
        self.assertEqual(unit.aprs_report_type, 'Analog')
        self.assertEqual(unit.aprs_rx, '144.39')
        self.assertEqual(unit.aprs_analog_ptt_mode, 'bot')

    def test_set_digital_aprs(self):
        unit = Memory(self.config)
        unit.set_digital_aprs('100', 'eot')
        self.assertEqual(unit.aprs_report_type, 'Digital')
        self.assertEqual(unit.aprs_digital_channel, '100')
        self.assertEqual(unit.aprs_digital_ptt_mode, 'eot')

        unit.set_digital_aprs(200, 'bot')
        self.assertEqual(unit.aprs_report_type, 'Digital')
        self.assertEqual(unit.aprs_digital_channel, '200')
        self.assertEqual(unit.aprs_digital_ptt_mode, 'bot')

    def test_set_scan_list(self):
        unit = Memory(self.config)
        unit.set_scan_list('my_list')
        self.assertEqual(unit.scan_list, 'my_list')

    def test_set_rx_group_list(self):
        unit = Memory(self.config)
        unit.set_rx_group_list('my_list')
        self.assertEqual(unit.rx_group_list, 'my_list')

    def test_normalize_bandwidth(self):
        self.assertEqual(Memory.normalize_bandwidth('wide'), '25K')
        self.assertEqual(Memory.normalize_bandwidth('narrow'), '12.5K')
        self.assertEqual(Memory.normalize_bandwidth('30K'), '12.5K')

    def test_normalize_power(self):
        self.assertEqual(Memory.normalize_power('slow'), 'Low')
        self.assertEqual(Memory.normalize_power('low'), 'Low')
        self.assertEqual(Memory.normalize_power('medlo'), 'Med')
        self.assertEqual(Memory.normalize_power('med'), 'Med')
        self.assertEqual(Memory.normalize_power('medhi'), 'Med')
        self.assertEqual(Memory.normalize_power('high'), 'High')
        self.assertEqual(Memory.normalize_power('shigh'), 'Turbo')
        self.assertEqual(Memory.normalize_power('turbo'), 'Turbo')

    def test_normalize_sql_mode(self):
        # Squelch type: none, tone, tsql, dcs, off
        self.assertEqual(Memory.normalize_sql_mode(None), 'Carrier')
        self.assertEqual(Memory.normalize_sql_mode('off'), 'Carrier')
        self.assertEqual(Memory.normalize_sql_mode('none'), 'Carrier')
        self.assertEqual(Memory.normalize_sql_mode('tone'), 'CTCSS/DCS')
        self.assertEqual(Memory.normalize_sql_mode('tsql'), 'CTCSS/DCS')
        self.assertEqual(Memory.normalize_sql_mode('dcs'), 'CTCSS/DCS')

    def test_normalize_frequency(self):
        self.assertEqual(Memory.normalize_frequency('123'), '123.00000')
        self.assertEqual(Memory.normalize_frequency(123), '123.00000')
        self.assertEqual(Memory.normalize_frequency('123.3'), '123.30000')
        self.assertEqual(Memory.normalize_frequency('123.300'), '123.30000')
        self.assertEqual(Memory.normalize_frequency('0.250'), '0.25000')
        self.assertEqual(Memory.normalize_frequency(0.3905), '0.39050')

    def test_normalize_chan_type(self):
        self.assertEqual(Memory.normalize_chan_type('analog_tx'), 'A+D TX A')
        self.assertEqual(Memory.normalize_chan_type('digital_tx'), 'D+A TX D')
        self.assertEqual(Memory.normalize_chan_type('analog'), 'A-Analog')
        self.assertEqual(Memory.normalize_chan_type('digital'), 'D-Digital')
        self.assertEqual(Memory.normalize_chan_type('something_else'), None)

    def test_normalize_dmr_mode(self):
        self.assertEqual(Memory.normalize_dmr_mode('simplex'), '0')
        self.assertEqual(Memory.normalize_dmr_mode('repeater'), '1')
        self.assertEqual(Memory.normalize_dmr_mode('dual_slot'), '2')
        self.assertEqual(Memory.normalize_dmr_mode('anything_else'), '0')

    def test_normalize_dmr_calltype(self):
        self.assertEqual(Memory.normalize_dmr_calltype('group'), 'Group Call')
        self.assertEqual(Memory.normalize_dmr_calltype('private'), 'Private Call')
        self.assertEqual(Memory.normalize_dmr_calltype('anything_else'), None)

    def test_normalize_dmr_tx_permit(self):
        self.assertEqual(Memory.normalize_dmr_tx_permit('always'), 'Always')
        self.assertEqual(Memory.normalize_dmr_tx_permit('same_cc'), 'Same Color Code')
        self.assertEqual(Memory.normalize_dmr_tx_permit('diff_cc'), 'Different Color Code')
        self.assertEqual(Memory.normalize_dmr_tx_permit('free'), 'Free Channel')
        self.assertEqual(Memory.normalize_dmr_tx_permit('all_others'), 'Off')

    def test_choose_headers_with_v12(self):
        config = OpenStruct(self.config)
        config.fw_version = '1.2'

        unit = Memory(config)
        print(f'{Memory._headers=}')
        self.assertTrue("No." in Memory._headers)
        self.assertTrue("Channel Name" in Memory._headers)
        self.assertTrue("Receive Frequency" in Memory._headers)
        self.assertTrue("Transmit Frequency" in Memory._headers)
        self.assertTrue("Channel Type" in Memory._headers)
        self.assertTrue("Transmit Power" in Memory._headers)
        self.assertTrue("Band Width" in Memory._headers)
        self.assertTrue("CTCSS/DCS Decode" in Memory._headers)
        self.assertTrue("CTCSS/DCS Encode" in Memory._headers)
        self.assertTrue("Contact" in Memory._headers)
        self.assertTrue("Contact Call Type" in Memory._headers)
        self.assertTrue("Radio ID" in Memory._headers)
        self.assertTrue("Busy Lock/TX Permit" in Memory._headers)
        self.assertTrue("Squelch Mode" in Memory._headers)
        self.assertTrue("Optional Signal" in Memory._headers)
        self.assertTrue("DTMF ID" in Memory._headers)
        self.assertTrue("2Tone ID" in Memory._headers)
        self.assertTrue("5Tone ID" in Memory._headers)
        self.assertTrue("PTT ID" in Memory._headers)
        self.assertTrue("Color Code" in Memory._headers)
        self.assertTrue("Slot" in Memory._headers)
        self.assertTrue("Scan List" in Memory._headers)
        self.assertTrue("Receive Group List" in Memory._headers)
        self.assertTrue("PTT Prohibit" in Memory._headers)
        self.assertTrue("Reverse" in Memory._headers)
        self.assertTrue("TDMA" in Memory._headers)
        self.assertFalse("TDMA Simplex" in Memory._headers)
        self.assertTrue("TDMA Adaptive" in Memory._headers)
        self.assertFalse("Slot Suit" in Memory._headers)
        self.assertTrue("AES Digital Encryption" in Memory._headers)
        self.assertTrue("Call Confirmation" in Memory._headers)
        self.assertTrue("Talk Around(Simplex)" in Memory._headers)
        self.assertTrue("Work Alone" in Memory._headers)
        self.assertTrue("Custom CTCSS" in Memory._headers)
        self.assertTrue("2TONE Decode" in Memory._headers)
        self.assertTrue("Ranging" in Memory._headers)
        self.assertTrue("Simplex" in Memory._headers)
        self.assertFalse("Through Mode" in Memory._headers)
        self.assertTrue("APRS RX" in Memory._headers)
        self.assertTrue("Analog APRS PTT Mode" in Memory._headers)
        self.assertTrue("Digital APRS PTT Mode" in Memory._headers)
        self.assertTrue("APRS Report Type" in Memory._headers)
        self.assertTrue("Digital APRS Report Channel" in Memory._headers)
        self.assertTrue("Correct Frequency[Hz]" in Memory._headers)
        self.assertTrue("SMS Confirmation" in Memory._headers)
        self.assertTrue("Exclude Channel From Roaming" in Memory._headers)
        self.assertTrue("DMR MODE" in Memory._headers)
        self.assertTrue("DataACK Disable" in Memory._headers)
        self.assertTrue("R5toneBot" in Memory._headers)
        self.assertTrue("R5ToneEot" in Memory._headers)
        self.assertFalse("Auto Scan" in Memory._headers)
        self.assertFalse("Ana Aprs Mute" in Memory._headers)
        self.assertFalse("Send Talker Alias" in Memory._headers)
        self.assertFalse("AnaAprsTxPath" in Memory._headers)

    def test_choose_headers_with_v30(self):
        config = OpenStruct(self.config)
        config.fw_version = '3.0'

        unit = Memory(config)
        print(f'{Memory._headers=}')
        self.assertTrue("No." in Memory._headers)
        self.assertTrue("Channel Name" in Memory._headers)
        self.assertTrue("Receive Frequency" in Memory._headers)
        self.assertTrue("Transmit Frequency" in Memory._headers)
        self.assertTrue("Channel Type" in Memory._headers)
        self.assertTrue("Transmit Power" in Memory._headers)
        self.assertTrue("Band Width" in Memory._headers)
        self.assertTrue("CTCSS/DCS Decode" in Memory._headers)
        self.assertTrue("CTCSS/DCS Encode" in Memory._headers)
        self.assertTrue("Contact" in Memory._headers)
        self.assertTrue("Contact Call Type" in Memory._headers)
        self.assertTrue("Radio ID" in Memory._headers)
        self.assertTrue("Busy Lock/TX Permit" in Memory._headers)
        self.assertTrue("Squelch Mode" in Memory._headers)
        self.assertTrue("Optional Signal" in Memory._headers)
        self.assertTrue("DTMF ID" in Memory._headers)
        self.assertTrue("2Tone ID" in Memory._headers)
        self.assertTrue("5Tone ID" in Memory._headers)
        self.assertTrue("PTT ID" in Memory._headers)
        self.assertTrue("Color Code" in Memory._headers)
        self.assertTrue("Slot" in Memory._headers)
        self.assertTrue("Scan List" in Memory._headers)
        self.assertTrue("Receive Group List" in Memory._headers)
        self.assertTrue("PTT Prohibit" in Memory._headers)
        self.assertTrue("Reverse" in Memory._headers)
        self.assertFalse("TDMA" in Memory._headers)
        self.assertTrue("TDMA Simplex" in Memory._headers)
        self.assertFalse("TDMA Adaptive" in Memory._headers)
        self.assertTrue("Slot Suit" in Memory._headers)
        self.assertTrue("AES Digital Encryption" in Memory._headers)
        self.assertTrue("Call Confirmation" in Memory._headers)
        self.assertTrue("Talk Around(Simplex)" in Memory._headers)
        self.assertTrue("Work Alone" in Memory._headers)
        self.assertTrue("Custom CTCSS" in Memory._headers)
        self.assertTrue("2TONE Decode" in Memory._headers)
        self.assertTrue("Ranging" in Memory._headers)
        self.assertFalse("Simplex" in Memory._headers)
        self.assertTrue("Through Mode" in Memory._headers)
        self.assertTrue("APRS RX" in Memory._headers)
        self.assertTrue("Analog APRS PTT Mode" in Memory._headers)
        self.assertTrue("Digital APRS PTT Mode" in Memory._headers)
        self.assertTrue("APRS Report Type" in Memory._headers)
        self.assertTrue("Digital APRS Report Channel" in Memory._headers)
        self.assertTrue("Correct Frequency[Hz]" in Memory._headers)
        self.assertTrue("SMS Confirmation" in Memory._headers)
        self.assertTrue("Exclude Channel From Roaming" in Memory._headers)
        self.assertTrue("DMR MODE" in Memory._headers)
        self.assertTrue("DataACK Disable" in Memory._headers)
        self.assertTrue("R5toneBot" in Memory._headers)
        self.assertTrue("R5ToneEot" in Memory._headers)
        self.assertTrue("Auto Scan" in Memory._headers)
        self.assertTrue("Ana Aprs Mute" in Memory._headers)
        self.assertTrue("Send Talker Alias" in Memory._headers)
        self.assertTrue("AnaAprsTxPath" in Memory._headers)

    def test_render(self):
        unit = Memory(self.config)
        unit.add('ICST 1', 146.56, 146.56)
        unit.set_channel_properties('analog', 'high', 'wide')
        unit.set_tone_properties(71.9, 71.9)
        unit.commit()

        unit = Memory(self.config)
        unit.add('DMR Simplex', 446.000, 446.000)
        unit.set_channel_properties('digital_tx', 'med', 'narrow')
        unit.set_dmr_properties('simplex', 2, 1, 99999)
        unit.set_contact_info('Simplex', 'group', 99)
        unit.commit()

        output = Memory.render()
        print(output, file=sys.stderr)
        self.assertEqual(output[0], Memory._headers)
        self.assertEqual(output[1][1], 'ICST 1')
        self.assertEqual(output[1][2], Memory.normalize_frequency(146.56))
        self.assertEqual(output[1][3], Memory.normalize_frequency(146.56))
        self.assertEqual(output[1][4], Memory.normalize_chan_type('analog'))
        self.assertEqual(output[1][5], Memory.normalize_power('high'))
        self.assertEqual(output[1][6], Memory.normalize_bandwidth('wide'))
        self.assertEqual(output[1][7], '71.9')
        self.assertEqual(output[1][8], '71.9')

        self.assertEqual(output[2][1], 'DMR Simplex')
        self.assertEqual(output[2][2], Memory.normalize_frequency(446.000))
        self.assertEqual(output[2][3], Memory.normalize_frequency(446.000))
        self.assertEqual(output[2][4], Memory.normalize_chan_type('digital_tx'))
        self.assertEqual(output[2][5], Memory.normalize_power('med'))
        self.assertEqual(output[2][6], Memory.normalize_bandwidth('narrow'))
        self.assertEqual(output[2][7], 'Off')
        self.assertEqual(output[2][8], 'Off')
        self.assertEqual(output[2][9], 'Simplex')
        self.assertEqual(output[2][10], Memory.normalize_dmr_calltype('group'))
        self.assertEqual(output[2][11], '99')
        self.assertEqual(output[2][12], '99999')
        self.assertEqual(output[2][20], '1')
        self.assertEqual(output[2][21], '2')
        self.assertEqual(output[2][46], Memory.normalize_dmr_mode('simplex'))

if __name__ == '__main__':
    unittest.main()
