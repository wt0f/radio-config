
from lib.ostruct import OpenStruct


ScanLists = list()

csv_labels = ("No.",
              "Scan List Name",
              "Scan Channel Member",
              "Scan Channel Member RX Frequency",
              "Scan Channel Member TX Frequency",
              "Scan Mode",
              "Priority Channel Select",
              "Priority Channel 1",
              "Priority Channel 1 RX Frequency",
              "Priority Channel 1 TX Frequency",
              "Priority Channel 2",
              "Priority Channel 2 RX Frequency",
              "Priority Channel 2 TX Frequency",
              "Revert Channel",
              "Look Back Time A[s]",
              "Look Back Time B[s]",
              "Dropout Delay Time[s]",
              "Dwell Time[s]")

_index = []

class ScanList:

    def __init__(self):
        self.name         = None
        self.members      = []
        self.members_rx   = []
        self.members_tx   = []
        self.a_channel    = None
        self.a_channel_rx = None
        self.a_channel_tx = None
        self.b_channel    = None
        self.b_channel_rx = None
        self.b_channel_tx = None

    def add(self, name):
        self.name = name

    def add_member(self, name, rx, tx):
        self.members.append(name)
        self.members_rx.append(str(rx))
        self.members_tx.append(str(tx))

    def set_channel_a(self, name, rx, tx):
        self.a_channel = name
        self.a_channel_rx = str(rx)
        self.a_channel_tx = str(tx)

    def set_channel_b(self, name, rx, tx):
        self.b_channel = name
        self.b_channel_rx = str(rx)
        self.b_channel_tx = str(tx)

    def commit(self):
        if self.name not in _index:
            _index.append(self.name)

            record = {'name': self.name,
                      'members': self.members,
                      'members_rx': self.members_rx,
                      'members_tx': self.members_tx,
                      'a_channel': self.a_channel,
                      'a_channel_rx': self.a_channel_rx,
                      'a_channel_tx': self.a_channel_tx,
                      'b_channel': self.b_channel,
                      'b_channel_rx': self.b_channel_rx,
                      'b_channel_tx': self.b_channel_tx}
            ScanLists.append(OpenStruct(record))


    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return Scanlist.name(args)
        return method

    @staticmethod
    def render():
        count = 1
        output = [csv_labels]
        for entry in ScanLists:
            output.append((count, entry.name,
                           "|".join(entry.members),
                           "|".join(entry.members_rx),
                           "|".join(entry.members_tx),
                           entry.a_channel,
                           entry.a_channel_rx,
                           entry.a_channel_tx,
                           entry.b_channel,
                           entry.b_channel_rx,
                           entry.b_channel_tx))
            count += 1

        return output
