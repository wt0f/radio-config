import logging

from .base import Base
from lib.ostruct import OpenStruct

Memories = list()

# Anytone CPS columns: https://www.repeaterbook.com/wiki/doku.php?id=anytone_d878uv_cps

class Memory(Base):
    @staticmethod
    def findByName(name):
        for entry in Memories:
            if entry['name'] == name:
                return entry

        return None

    @staticmethod
    def normalize_power(power):
        if power == "slow":
            power = "low"
        elif power == "medlo":
            power = "med"
        elif power == "medhi":
            power = "med"
        elif power == 'shigh':
            power = "turbo"
        return power.title()

    @staticmethod
    def normalize_sql_mode(mode):
        # Squelch type: none, tone, tsql, dcs, off
        if mode in ('tone', 'tsql', 'dcs'):
            return 'CTCSS/DCS'
        else:
          return 'Carrier'

    @staticmethod
    def normalize_frequency(freq):
        return "%.5f" % float(freq)

    @staticmethod
    def normalize_bandwidth(bw):
        if bw == 'narrow':
            return "12.5K"
        elif bw == 'wide':
            return "25K"
        else:
            return "12.5K"

    @staticmethod
    def normalize_chan_type(mode):
        if mode == 'analog_tx':
            return 'A+D TX A'
        elif mode == 'digital_tx':
            return 'D+A TX D'
        elif mode == 'analog':
            return 'A-Analog'
        elif mode == 'digital':
            return 'D-Digital'
        else:
            return None

    @staticmethod
    def normalize_dmr_mode(mode):
        if mode == 'simplex':
            return '0'
        elif mode == 'repeater':
            return '1'
        elif mode == 'dual_slot':
            return '2'
        else:
            return '0'

    @staticmethod
    def normalize_dmr_calltype(calltype):
        if calltype == 'group':
            return "Group Call"
        elif calltype == 'private':
            return "Private Call"
        else:
            return None

    @staticmethod
    def normalize_dmr_tx_permit(mode):
        if mode == 'always':
            return 'Always'
        elif mode == 'same_cc':
            return 'Same Color Code'
        elif mode == 'diff_cc':
            return 'Different Color Code'
        elif mode == 'free':
            return 'Free Channel'
        else:
            return 'Off'


    csv_labels = ({
                'attr': 'num',
                'header': 'No.'
              },  {
                'attr': 'name',
                'header': 'Channel Name',
              }, {
                'attr': 'rx',
                'header': 'Receive Frequency',
                'normalize': normalize_frequency,
              }, {
                'attr': 'tx',
                'header': 'Transmit Frequency',
                'normalize': normalize_frequency,
              }, {
                'attr': 'chan_type',
                'header': 'Channel Type',
                'normalize': normalize_chan_type,
              }, {
                'attr': 'power',
                'header': 'Transmit Power',
                'normalize': normalize_power,
              }, {
                'attr': 'bandwidth',
                'header': 'Band Width',
                'normalize': normalize_bandwidth,
              }, {
                'attr': 'rx_tone',
                'header': 'CTCSS/DCS Decode',
              }, {
                'attr': 'tx_tone',
                'header': 'CTCSS/DCS Encode',
              }, {
                'attr': 'contact',
                'header': 'Contact',
              }, {
                'attr': 'contact_type',
                'header': 'Contact Call Type',
                'normalize': normalize_dmr_calltype,
              }, {
                'attr': 'contact_tg',
                'header': 'Contact TG/DMR ID',
              }, {
                'attr': 'radio_id',
                'header': 'Radio ID',
              }, {
                'attr': 'tx_permit',
                'header': 'Busy Lock/TX Permit',
                'normalize': normalize_dmr_tx_permit,
              }, {
                'attr': 'squelch_mode',
                'header': 'Squelch Mode',
                'normalize': normalize_sql_mode,
              }, {
                'attr': 'opt_signal',
                'header': 'Optional Signal',
              }, {
                'attr': 'dtmf_id',
                'header': 'DTMF ID',
              }, {
                'attr': 'tone2_id',
                'header': '2Tone ID',
              }, {
                'attr': 'tone5_id',
                'header': '5Tone ID',
              }, {
                'attr': 'ptt_id',
                'header': 'PTT ID',
              }, {
                'attr': 'color_code',
                'header': 'Color Code',
              }, {
                'attr': 'slot',
                'header': 'Slot',
              }, {
                'attr': 'scan_list',
                'header': 'Scan List',
              }, {
                'attr': 'rx_group_list',
                'header': 'Receive Group List',
              }, {
                'attr': 'ptt_prohibit',
                'header': 'PTT Prohibit',
              }, {
              'attr': 'reverse',
                'header': 'Reverse',
              }, {
                'attr': 'tdma',
                'header': 'TDMA',
                'selector': '<3.0',
              }, {
                'attr': 'tdma',
                'header': 'TDMA Simplex',
                'selector': '>3.0',
              }, {
                'attr': 'tdma_adaptive',
                'header': 'TDMA Adaptive',
                'selector': '<3.0',
              }, {
                'attr': 'tdma_adaptive',
                'header': 'Slot Suit',
                'selector': '>3.0',
              }, {
                'attr': 'aes_encryption',
                'header': 'AES Digital Encryption',
              }, {
                'attr': 'encryption',
                'header': 'Digital Encryption',
              }, {
                'attr': 'call_confirm',
                'header': 'Call Confirmation',
              }, {
                'attr': 'talk_around',
                'header': 'Talk Around(Simplex)',
              }, {
                'attr': 'work_alone',
                'header': 'Work Alone',
              }, {
                'attr': 'custom_tone',
                'header': 'Custom CTCSS',
              }, {
                'attr': 'tone2_decode',
                'header': '2TONE Decode',
              }, {
                'attr': 'ranging',
                'header': 'Ranging',
              }, {
                'attr': 'simplex',
                'header': 'Simplex',
                'selector': '<3.0',
              }, {
                'attr': 'simplex',
                'header': 'Through Mode',
                'selector': '>3.0',
              }, {
                'attr': 'aprs_rx',
                'header': 'APRS RX',
              }, {
                'attr': 'aprs_analog_ptt_mode',
                'header': 'Analog APRS PTT Mode',
              }, {
                'attr': 'aprs_digital_ptt_mode',
                'header': 'Digital APRS PTT Mode',
              }, {
                'attr': 'aprs_report_type',
                'header': 'APRS Report Type',
              }, {
                'attr': 'aprs_digital_channel',
                'header': 'Digital APRS Report Channel',
              }, {
                'attr': 'freq_correction',
                'header': 'Correct Frequency[Hz]',
              }, {
                'attr': 'sms_confirmation',
                'header': 'SMS Confirmation',
              }, {
                'attr': 'roaming_exclude',
                'header': 'Exclude Channel From Roaming'
              }, {
                'attr': 'dmr_mode',
                'header': 'DMR MODE',
                'normalize': normalize_dmr_mode,
              }, {
                'attr': 'disable_dataack',
                'header': 'DataACK Disable',
              }, {
                'attr': 'r5tone_bot',
                'header': 'R5toneBot',
              }, {
                'attr': 'r5tone_eot',
                'header': 'R5ToneEot',
              }, {
                'attr': 'auto_scan',
                'header': 'Auto Scan',
                'selector': '>3.0',
              }, {
                'attr': 'aprs_mute',
                'header': 'Ana Aprs Mute',
                'selector': '>3.0',
              }, {
                'attr': 'talker_alias',
                'header': 'Send Talker Alias',
                'selector': '>3.0',
              }, {
                'attr': 'aprs_tx_path',
                'header': 'AnaAprsTxPath',
                'selector': '>3.0',
              })

    _headers = list()
    _attrs = list()
    _index = list()

    def __init__(self, config):
        self.name                  = None
        self.rx                    = None
        self.tx                    = None
        self.chan_type             = "analog"  # A+D TX A, D+A TX D, D-Digital
        self.power                 = "low"       # low, Mid, High, Turbo
        self.bandwidth             = "wide"       # 12.5K
        self.rx_tone               = "Off"       # CTCSS code
        self.tx_tone               = "Off"       # CTCSS code
        self.rx_dcs_code           = "Off"       # DCS code
        self.tx_dcs_code           = "Off"       # DCS code
        self.rx_dcs_polarity       = 'N'
        self.tx_dcs_polarity       = 'N'
        self.contact               = None
        self.contact_type          = None
        self.contact_tg            = None
        self.radio_id              = None
        self.tx_permit             = "Off"       # Same Color Code, Different Color Code, Always, ChannelFree
        self.squelch_mode          = "Carrier"   # CTCSS/DCS,
        self.opt_signal            = "Off"       # On
        self.dtmf_id               = "1"
        self.tone2_id              = "1"
        self.tone5_id              = "1"
        self.ptt_id                = "Off"       # On
        self.color_code            = "1"
        self.slot                  = "1"
        self.scan_list             = "None"
        self.rx_group_list         = "None"
        self.ptt_prohibit          = "Off"
        self.reverse               = "Off"
        self.tdma                  = "Off"
        self.tdma_adaptive         = "Off"
        self.aes_encryption        = "Normal Encryption"
        self.encryption            = "Off"
        self.call_confirm          = "Off"
        self.talk_around           = "Off"
        self.work_alone            = "Off"
        self.custom_tone           = "251.1"
        self.tone2_decode          = "1"
        self.ranging               = "Off"
        self.simplex               = "Off"
        self.aprs_rx               = "Off"
        self.aprs_analog_ptt_mode  = "Off"       # End of Transmission, Start of Transmission
        self.aprs_digital_ptt_mode = "Off"
        self.aprs_report_type      = "Off"       # Analog, Digital
        self.aprs_digital_channel  = "1"
        self.freq_correction       = "0"
        self.sms_confirmation      = "Off"
        self.roaming_exclude       = "0"
        self.dmr_mode              = "0"
        self.disable_dataack       = "0"
        self.r5tone_bot            = "0"
        self.r5tone_eot            = "0"
        self.auto_scan             = "0"
        self.aprs_mute             = "0"
        self.talker_alias          = "0"
        self.aprs_tx_path          = "0"

        self.choose_headers(config)

    def add(self, name, rx, tx):
        self.name = name
        self.rx = str(rx)
        self.tx = str(tx)

    def set_tx_inhibit(self, val):
        self.ptt_prohibit = self.bool_value(val)

    def set_channel_properties(self, chan_type, power, bw):
        self.chan_type = chan_type
        self.power = power
        self.bandwidth = bw

    def set_squelch_properties(self, mode):
        self.squelch_mode = mode

    def set_tone_properties(self, rx_tone, tx_tone):
        if (rx_tone is None) or (rx_tone == 'Off'):
          self.rx_tone = 'Off'
        else:
          self.rx_tone = "%.1f" % float(rx_tone)

        if (tx_tone is None) or (tx_tone == 'Off'):
          self.tx_tone = 'Off'
        else:
          self.tx_tone = "%.1f" % float(tx_tone)

    def set_dcs_properties(self, rx_code, tx_code, rx_polarity, tx_polarity):
        if (rx_code is None) or (rx_code == 'Off'):
          self.rx_tone = 'Off'
        else:
          self.rx_tone = "D%03d%s" % (int(rx_code), rx_polarity)

        if (tx_code is None) or (tx_code == 'Off'):
          self.tx_tone = 'Off'
        else:
          self.tx_tone = "D%03d%s" % (int(tx_code), tx_polarity)

    def set_contact_info(self, contact, contact_type, tg):
        self.contact = contact
        self.contact_type = contact_type
        self.contact_tg = str(tg)

    def set_dmr_properties(self, mode, slot, cc, radio_id):
        self.dmr_mode = mode
        self.slot = str(slot)
        self.color_code = str(cc)
        self.radio_id = str(radio_id)

    def set_dmr_tx_allow(self, mode):
        self.tx_permit = mode

    def set_analog_aprs(self, freq, ptt_mode=None):
        self.aprs_report_type = "Analog"
        self.aprs_rx = str(freq)
        self.aprs_analog_ptt_mode = ptt_mode

    def set_digital_aprs(self, chan=None, ptt_mode=None):
        self.aprs_report_type = "Digital"
        self.aprs_digital_channel = str(chan)
        self.aprs_digital_ptt_mode = ptt_mode

    def set_scan_list(self, name):
        self.scan_list = name

    def set_rx_group_list(self, name):
        self.rx_group_list = name

    def commit(self):
        if self.name not in Memory._index:
            Memory._index.append(self.name)

            record = {'name': self.name,
                      'rx': self.rx,
                      'tx': self.tx,
                      'chan_type': self.chan_type,
                      'power': self.power,
                      'bandwidth': self.bandwidth,
                      'rx_tone': self.rx_tone,
                      'tx_tone': self.tx_tone,
                      'contact': self.contact,
                      'contact_type': self.contact_type,
                      'contact_tg': self.contact_tg,
                      'radio_id': self.radio_id,
                      'tx_permit': self.tx_permit,
                      'squelch_mode': self.squelch_mode,
                      'opt_signal': self.opt_signal,
                      'dtmf_id': self.dtmf_id,
                      'tone2_id': self.tone2_id,
                      'tone5_id': self.tone5_id,
                      'ptt_id': self.ptt_id,
                      'color_code': self.color_code,
                      'slot': self.slot,
                      'scan_list': self.scan_list,
                      'rx_group_list': self.rx_group_list,
                      'ptt_prohibit': self.ptt_prohibit,
                      'reverse': self.reverse,
                      'tdma': self.tdma,
                      'tdma_adaptive': self.tdma_adaptive,
                      'aes_encryption': self.aes_encryption,
                      'encryption': self.encryption,
                      'call_confirm': self.call_confirm,
                      'talk_around': self.talk_around,
                      'work_alone': self.work_alone,
                      'custom_tone': self.custom_tone,
                      'tone2_decode': self.tone2_decode,
                      'ranging': self.ranging,
                      'simplex': self.simplex,
                      'aprs_rx': self.aprs_rx,
                      'aprs_analog_ptt_mode': self.aprs_analog_ptt_mode,
                      'aprs_digital_ptt_mode': self.aprs_digital_ptt_mode,
                      'aprs_report_type': self.aprs_report_type,
                      'aprs_digital_channel': self.aprs_digital_channel,
                      'freq_correction': self.freq_correction,
                      'sms_confirmation': self.sms_confirmation,
                      'roaming_exclude': self.roaming_exclude,
                      'dmr_mode': self.dmr_mode,
                      'disable_dataack': self.disable_dataack,
                      'r5tone_bot': self.r5tone_bot,
                      'r5tone_eot': self.r5tone_eot,
                      'auto_scan': self.auto_scan,
                      'aprs_mute': self.aprs_mute,
                      'talker_alias': self.talker_alias,
                      'aprs_tx_path': self.aprs_tx_path,
                    }
            Memories.append(OpenStruct(record))

    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return Memory.name(args)
        return method

    def choose_headers(self, config):
        if len(Memory._attrs) == 0:
            fw_version = config.fw_version
            headers = list()

            for definition in Memory.csv_labels:
                if 'selector' in definition.keys():
                    if definition['selector'].startswith('>'):
                        min_vers = float(definition['selector'][1:])
                        if float(config.fw_version) >= min_vers:
                            Memory._headers.append(definition['header'])
                            Memory._attrs.append(definition)
                    elif definition['selector'].startswith('<'):
                        max_vers = float(definition['selector'][1:])
                        if float(config.fw_version) < max_vers:
                            Memory._headers.append(definition['header'])
                            Memory._attrs.append(definition)
                    elif definition['selector'].startswith('='):
                        vers = float(definition['selector'][1:])
                        if float(config.fw_version) == vers:
                            Memory._headers.append(definition['header'])
                            Memory._attrs.append(definition['attr'])
                    else:
                        raise RuntimeError(f"Bad selector in Zones cvs_labels definitons: {definition['selector'][0]}")
                else:
                    # No selector means "all"
                    Memory._headers.append(definition['header'])
                    Memory._attrs.append(definition)



    @staticmethod
    def render():
        count = 1
        output = [ Memory._headers ]
        for entry in Memories:
            logging.debug(f'{entry.keys()=}')
            logging.debug(f'{Memory._attrs=}')
            datum = []
            for attr in Memory._attrs:
                if attr['attr'] == "num":
                    datum.append(count)
                else:
                    if 'normalize' in attr:
                        func = attr['normalize']
                        datum.append(func(entry[attr['attr']]))
                    else:
                        datum.append(entry[attr['attr']])
            output.append(datum)
            count += 1

        return output
