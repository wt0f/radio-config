#!/usr/bin/env python3
import sys
import unittest

from .talkgroup import Talkgroup, Talkgroups, csv_labels, _index
from lib.ostruct import OpenStruct

class TestTalkgroup(unittest.TestCase):

    def tearDown(self):
        Talkgroups.clear()
        _index.clear()

    def test_init_obj(self):
        unit = Talkgroup()
        self.assertEqual(unit.id, None)
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.calltype, None)
        self.assertEqual(unit.alert, "None")

    def test_add(self):
        unit = Talkgroup()
        unit.add('99', 'Simplex', 'group', 'alert')

        self.assertEqual(unit.id, '99')
        self.assertEqual(unit.name, 'Simplex')
        self.assertEqual(unit.calltype, 'group')
        self.assertEqual(unit.alert, 'alert')

    def test_normalize_calltype(self):
        self.assertTrue(Talkgroup.normalize_calltype('group'), 'Group Call')
        self.assertTrue(Talkgroup.normalize_calltype('private'), 'Private Call')
        self.assertTrue(Talkgroup.normalize_calltype('unknown'), 'None')

    def test_findByName(self):
        unit = Talkgroup()
        unit.add('1', 'Local 1', 'group', 'none')
        unit = Talkgroup()
        unit.add('2', 'Local 2', 'group', 'none')
        unit = Talkgroup()
        unit.add('99', 'Simplex', 'group', 'none')
        unit = Talkgroup()
        unit.add('9998', 'Parrot', 'private', 'none')

        self.assertIsInstance(Talkgroup.findByName('Local 1'), OpenStruct)
        self.assertEqual(Talkgroup.findByName('Local 2').id, '2')
        self.assertEqual(Talkgroup.findByName('Simplex').name, 'Simplex')
        self.assertEqual(Talkgroup.findByName('Parrot').calltype, 'private')
        with self.assertRaises(RuntimeError):
            Talkgroup.findByName('foobar')

    def test_render_single_id(self):
        unit = Talkgroup()
        unit.add('99', 'Simplex', 'group', 'alert')

        output = Talkgroup().render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, '99', 'Simplex', 'Group Call', 'alert'))

    def test_render_multi_id(self):
        unit = Talkgroup()
        unit.add('1', 'Local 1', 'group', 'none')
        unit.add('2', 'Local 2', 'group', 'none')
        unit.add('99', 'Simplex', 'group', 'none')
        unit.add('9998', 'Parrot', 'private', 'none')

        output = Talkgroup().render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1] == (1, '1', 'Local 1', 'Group Call', 'none'))
        self.assertTrue(output[2] == (2, '2', 'Local 2', 'Group Call', 'none'))
        self.assertTrue(output[3] == (3, '99', 'Simplex', 'Group Call', 'none'))
        self.assertTrue(output[4] == (4, '9998', 'Parrot', 'Private Call', 'none'))

    def test_findByName_duplicate_id(self):
        unit = Talkgroup()
        unit.add('2', 'Local 2', 'group', 'none')
        unit.add('2', 'Test 2', 'group', 'none')

        self.assertEqual(Talkgroup.findByName('Local 2').id, '2')
        self.assertEqual(Talkgroup.findByName('Test 2').id, '2')


if __name__ == '__main__':
    unittest.main()
