import logging
from lib.ostruct import OpenStruct


Talkgroups = list()

csv_labels = ("No.",
              "Radio ID",
              "Name",
              "Call Type",
              "Call Alert")

_index = []

class Talkgroup:

    def __init__(self):
        self.id         = None
        self.name       = None
        self.calltype   = None
        self.alert      = "None"

    def add(self, id, name, ctyp, alert):
        self.id = str(id)
        self.name = name
        self.calltype = ctyp
        self.alert = alert

        if self.id not in _index:
            _index.append(self.id)

        record = {'id': self.id, 'name': self.name,
                  'calltype': self.calltype, 'alert': self.alert}
        Talkgroups.append(OpenStruct(record))

    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return Talkgroup.name(args)
        return method

    @staticmethod
    def findByName(name):
        for entry in Talkgroups:
            if entry['name'] == name:
                return entry

        raise RuntimeError("Talkgroup %s not found" % name)

    @staticmethod
    def normalize_calltype(calltype):
        if calltype == 'group':
            return "Group Call"
        elif calltype == 'private':
            return "Private Call"
        else:
            return "None"


    @staticmethod
    def render():
        count = 1
        output = [csv_labels]
        for entry in Talkgroups:
            output.append((count, entry.id, entry.name,
                           Talkgroup.normalize_calltype(entry.calltype),
                           entry.alert))
            count += 1

        return output
