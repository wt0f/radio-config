"""Anytone radios"""

import sys
import logging
import csv
import re
import pdb

from lib.ostruct import OpenStruct

from .radio_id import RadioID
from .zone import Zone
from .memory import Memory
from .talkgroup import Talkgroup
from .grouplist import GroupList
from .scanlist import ScanList
from .repeater_offset import RepeaterOffset

class Adapter:
    def __init__(self, config):
        self.radio_type = 'anytone'
        self.radio_config = OpenStruct(config)

        self.defaults = {
            'bands': [
                {'range': '136-170'},
                {'range': '400-470'},
            ]
        }

    def load_codeplug_data(self, data):
        logging.info(f"load_codeplug_data({data=})")
        self.codeplug_data = data

        # Filter out any stations that can not be used by the radio
        station_to_remove = []
        for station_name in self.codeplug_data.stations.data.keys():
            try:
                rx = self.codeplug_data.stations.data[station_name]['rx']
                tx = self.codeplug_data.stations.data[station_name]['tx']
                if not (self.freq_in_band(rx) and self.freq_in_band(tx)):
                    station_to_remove.append(station_name)
            except KeyError as e:
                logging.critical(f"{e} for station {station_name}")
                raise e

        for station_name in station_to_remove:
            logging.info(f"Removing station {station_name} due to outside of bands")
            self.codeplug_data.stations.remove_station(station_name)
            self.codeplug_data.scanlists.remove_station(station_name)

    def freq_in_band(self, freq):
        bands = self.defaults['bands']
        if 'bands' in self.radio_config.settings:
            bands = self.radio_config.settings.bands

        for band in bands:
            lower, upper = re.split(r'\s*-\s*', band['range'])
            if float(lower) <= float(freq) <= float(upper):
                return True
        return False

    def generate_codeplug(self, dir):
        logging.debug(f"generate_codeplug({dir})")

        csv_list = []

        logging.info("Creating RadioIDList.CSV")
        for query_term in self.radio_config.settings.dmr.radio_ids.tags:
            for item in self.codeplug_data.radio_ids.query(query_term):
                RadioID().add(item['name'], item['id'])
        rows = RadioID.render()
        Adapter.write_csv_file(f"{dir}/RadioIDList.CSV", rows)
        csv_list.append("RadioIDList.CSV")

        logging.info("Creating ContactTalkGroup.CSV")
        for query_term in self.radio_config.settings.dmr.talkgroups.tags:
            for item in self.codeplug_data.talkgroups.query(query_term):
                alert = 'alert' in item and item['alert'] or "None"
                Talkgroup().add(item['talkgroup'], item['name'],
                                item['calltype'], alert)
        rows = Talkgroup.render()
        Adapter.write_csv_file(f"{dir}/ContactTalkGroups.CSV", rows)
        csv_list.append("ContactTalkGroups.CSV")

        logging.info("Creating ReceiveGroupCallList.CSV")
        for query_term in self.radio_config.settings.dmr.receivegroups.tags:
            logging.info(f"Generating rx groups using tag {query_term}")
            for item in self.codeplug_data.grouplists.query(query_term):
                logging.info(f"Processing rx grouplist: {item=}")
                grouplist = GroupList()
                grouplist.add(item['name'])
                for tg_name in item['members']:
                    tg_info = Talkgroup().findByName(tg_name)
                    grouplist.add_member(tg_name, tg_info.id)
                grouplist.commit()
        rows = GroupList.render()
        Adapter.write_csv_file(f"{dir}/ReceiveGroupCallList.CSV", rows)
        csv_list.append("ReceiveGroupCallList.CSV")

        logging.info("Creating zone file")
        for entry in self.radio_config.zones:
            logging.info(f"Building out zone {entry['name']=}")
            zone = Zone(self.radio_config)
            zone.add(entry['name'])

            # gather all the channels and add them to the memories and zone
            for query_term in entry['tags']:
                for sta in self.codeplug_data.stations.query(query_term):
                    logging.info(f"Processing station: {sta['name']}")

                    # add station to list of memories
                    mem = Memory(self.radio_config)
                    try:
                        mem.add(sta['name16'], sta['rx'], sta['tx'])
                        if 'scanlist' in sta:
                            mem.set_scan_list(sta['scanlist'])
                        if 'tx_inhibit' in sta:
                            mem.set_tx_inhibit(sta['tx_inhibit'])


                        # set_analog_aprs(self, freq, ptt_mode=None):
                        # set_digital_aprs(self, chan=None, ptt_mode=None):

                        if sta['chan_mode'] == 'fm':
                            logging.debug(f"Setting station properties for {sta['name']}")
                            mem.set_channel_properties("analog", sta['power'], "wide")
                            logging.debug(f"Setting SQL property: {sta['fm_sql_mode']}")
                            mem.set_squelch_properties(sta['fm_sql_mode'])
                            if 'fm_rx_tone' in sta or 'fm_tx_tone' in sta:
                                rx_tone = 'fm_rx_tone' in sta and sta['fm_rx_tone'] or 'Off'
                                tx_tone = 'fm_tx_tone' in sta and sta['fm_tx_tone'] or 'Off'
                                logging.debug(f"Setting tone property: {rx_tone}/{tx_tone}")
                                mem.set_tone_properties(rx_tone, tx_tone)
                            if 'fm_rx_dcs_code' in sta or 'fm_tx_dcs_code' in sta:
                                rx_code = 'fm_rx_dcs_code' in sta and str(sta['fm_rx_dcs_code']) or 'Off'
                                tx_code = 'fm_tx_dcs_code' in sta and str(sta['fm_tx_dcs_code']) or 'Off'
                                rx_polarity = 'fm_rx_dcs_polarity' in sta and sta['fm_rx_dcs_polarity'].upper() or 'N'
                                tx_polarity = 'fm_tx_dcs_polarity' in sta and sta['fm_tx_dcs_polarity'].upper() or 'N'
                                logging.debug(f"Setting DCS code: {rx_code}{rx_polarity}/{tx_code}{tx_polarity}")
                                mem.set_dcs_properties(rx_code, tx_code, rx_polarity, tx_polarity)

                        if sta['chan_mode'] == 'dmr':
                            logging.debug(f"Setting station properties for {sta['name']}")
                            mem.set_channel_properties("digital", sta['power'], "narrow")

                            radio_id = RadioID().findByName(sta['dmr_radio_id'])
                            logging.debug(f"Setting DMR properties: {sta['dmr_mode']},slot {sta['dmr_slot']}, cc {sta['dmr_cc']}, id {radio_id.id}")
                            mem.set_dmr_properties(sta['dmr_mode'],
                                                   sta['dmr_slot'],
                                                   sta['dmr_cc'],
                                                   radio_id.id)
                            logging.debug(f"Setting TX allow: {sta['dmr_tx_allow']}")
                            mem.set_dmr_tx_allow(sta['dmr_tx_allow'])

                            try:
                                tg_info = Talkgroup().findByName(sta['dmr_talkgroup'])
                            except RuntimeError as e:
                                logging.critical(f"{e} for {sta['name']} in {self.radio_config.name}")
                            logging.debug(f"Setting TG info: {tg_info.name}, {tg_info.calltype}, {tg_info.id}")
                            mem.set_contact_info(tg_info.name, tg_info.calltype,
                                                 tg_info.id)
                            if 'dmr_rx_group_list' in sta:
                                logging.debug(f"Setting RX grouplist: {sta['dmr_rx_group_list']}")
                                mem.set_rx_group_list(sta['dmr_rx_group_list'])

                        if 'chan_type' in sta:
                            logging.debug(f"Setting channel type: {sta['chan_type']}")
                            mem.chan_type = sta['chan_type']

                        # don't save memory if a dstar or fusion channel
                        if sta['chan_mode'] not in ('dstar', 'fusion'):
                            logging.debug("Adding {sta['name']} to memories")
                            mem.commit()

                            # add station to zone
                            logging.debug(f"Adding {sta['name']} to zone")
                            zone.add_member(sta['name16'], sta['rx'], sta['tx'])

                            # add ordering information to zone if available
                            if 'sequence' in sta:
                                logging.debug(f"Adding sort sequence: {sta['sequence']}")
                                zone.add_sort(sta['sequence'], sta['name16'])
                    except Exception as e:
                        import traceback

                        logging.critical(f"Exception hit for {sta['name']}")
                        logging.critical(f"{type(e)}: {e}")
                        tb = sys.exc_info()[2]
                        logging.critical(traceback.print_tb(tb))
                        logging.critical(f"Station data: {sta=}")
                        sys.exit(1)

            zone.commit()
        rows = Zone.render()
        Adapter.write_csv_file(f"{dir}/Zone.CSV", rows)
        csv_list.append("Zone.CSV")

        logging.info("Creating Channel.CSV")
        rows = Memory.render()
        Adapter.write_csv_file(f"{dir}/Channel.CSV", rows)
        csv_list.append("Channel.CSV")

        logging.info("Creating ScanList.CSV")
        for entry in self.radio_config.scanlists:
            logging.info(f"Creating {entry['name']} scanlist")
            scanlist = ScanList()
            scanlist.add(entry['name'])
            for query_term in entry['tags']:
                for item in self.codeplug_data.scanlists.query(query_term):
                    for sta_name in item['members']:
                        sta = Memory.findByName(sta_name)
                        if sta:
                            scanlist.add_member(sta_name, sta.rx, sta.tx)
                        else:
                            logging.warning(f"No memory '{sta_name}' for {entry['name']} scanlist in {self.radio_config.name}")

            # set_channel_a(self, name, rx, tx)
            # set_channel_b(self, name, rx, tx)
            scanlist.commit()
        #pdb.set_trace()
        rows = ScanList.render()
        Adapter.write_csv_file(f"{dir}/ScanList.CSV", rows)
        csv_list.append("ScanList.CSV")

        logging.info("Creating AutoRepeaterOffsetFrequencys.CSV")
        repeater_offsets = RepeaterOffset()
        if 'repeater_offsets' in self.radio_config.settings.keys():
            for band in self.radio_config.settings.repeater_offsets.keys():
                repeater_offsets(band, self.radio_config.settings.repeater_offsets[band])
        rows = repeater_offsets.render()
        Adapter.write_csv_file(f"{dir}/AutoRepeaterOffsetFrequencys.CSV", rows)
        csv_list.append("AutoRepeaterOffsetFrequencys.CSV")

        # write the manifest file for the CPS
        with open(f"{dir}/Standard.LST", 'w') as mfh:
            mfh.write(f"{len(csv_list)}\n")
            count = 0
            for fname in csv_list:
                mfh.write(f'{count},"{fname}"\n')
                count += 1
            mfh.write("\n")

    @staticmethod
    def write_csv_file(filename, rows):
        with open(filename, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"',
                                   quoting=csv.QUOTE_ALL)
            for row in rows:
                csvwriter.writerow(row)
