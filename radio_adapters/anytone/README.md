Radio Adapter: anytone
======================

Status: beta

Tested radios
----------------

- Anytone
    - AT-D878
    - AT-D578

Supported features
------------------

- Basic frequency, duplex, offset, tone and DCS code

TODO
----

-
Notes
-----


Standard configuration
----------------------

```yaml
configuration:
  name: radio-name
  radio_type: anytone
  model: [at878|at578]
  fw_version: 3.0
  settings:
    name: name6
    bands:
      - range: 136-174
      - range: 400-480

  zones:
    ....
```

Anytone has consistantly revised the data included in a few of the import
files. As a result, it has become necessary to include the following
settings in any of the Anytone radio configuration files.

|     Name     |   Accepted Values                                     |
|--------------|-------------------------------------------------------|
| `model`      | Model of the Anytone radio. Either `at578` or `at878` |
| `fw_version` | Version of firmware installed on the radio            |

Other Anytone radios may also be supported by this adapter, but samples of
the exported files need to be examined to determine compatibility / feasibility
of supporting the radio with this adapter. If you find that there is a
radio that you would like to have supported, please create an issue and
include all the exported CSV files.

The `fw_version` value will very, but it is used to determine what CSV
headers need to be exported.

