
import copy

csv_labels = ("No.",
              "Offset Frequency")

band_offsets = {'2M': 0.600, '70CM': 5.0, '1.25M': 1.6}

class RepeaterOffset:

    def __init__(self):
        self.data = copy.deepcopy(band_offsets)

    def add(self, band, offset):
        self.data[band] = float(offset)

    def render(self):
        count = 1
        output = [csv_labels]
        for band in self.data.keys():
            output.append((count, self.pretty_frequency(self.data[band])))
            count += 1
        return output

    @staticmethod
    def pretty_frequency(freq):
        if freq < 1.0:
            return "%.2f KHz" % (freq * 1000.0)
        else:
            return "%.5f MHz" % freq
