#!/usr/bin/env python3
import sys
import re
import unittest

from lib.ostruct import OpenStruct
from .zone import Zone, Zones

class TestGroupList(unittest.TestCase):

    config = OpenStruct({
        'name': 'Anytone test harness',
        'radio_type': 'anytone',
        'model': 'at578',
        'fw_version': '1.0',
        'settings': {
            'bands': [
                { 'range': '136-174' },
                { 'range': '400-480' },
            ]
        }
    })

    def tearDown(self):
        Zones.clear()
        Zone._index.clear()
        Zone._headers.clear()
        Zone._attrs.clear()

    def test_init_obj(self):
        unit = Zone(self.config)
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.members, list())
        self.assertEqual(unit.members_rx, dict())
        self.assertEqual(unit.members_tx, dict())
        self.assertEqual(unit.a_channel, None)
        self.assertEqual(unit.a_channel_rx, None)
        self.assertEqual(unit.a_channel_tx, None)
        self.assertEqual(unit.b_channel, None)
        self.assertEqual(unit.b_channel_rx, None)
        self.assertEqual(unit.b_channel_tx, None)
        self.assertEqual(unit.sort_index, dict())

    def test_add(self):
        unit = Zone(self.config)
        unit.add('Home')
        self.assertEqual(unit.name, 'Home')

    def test_add_member(self):
        unit = Zone(self.config)
        unit.add_member('freq1', '100.30', '200.60')
        unit.add_member('freq2', 444.123, 448.987)

        self.assertTrue('freq1' in unit.members)
        self.assertTrue('freq2' in unit.members)
        self.assertEqual(unit.members_rx['freq1'], '100.30')
        self.assertEqual(unit.members_tx['freq1'], '200.60')
        self.assertEqual(unit.members_rx['freq2'], '444.123')
        self.assertEqual(unit.members_tx['freq2'], '448.987')

    def test_set_channel_a(self):
        unit = Zone(self.config)

        unit.set_channel_a('chan1', '146.52', '146.52')
        self.assertEqual(unit.a_channel, 'chan1')
        self.assertEqual(unit.a_channel_rx, '146.52')
        self.assertEqual(unit.a_channel_tx, '146.52')

        unit.set_channel_a('eoc rpt', 444.000, 449.000)
        self.assertEqual(unit.a_channel, 'eoc rpt')
        self.assertEqual(unit.a_channel_rx, '444.0')
        self.assertEqual(unit.a_channel_tx, '449.0')

    def test_set_channel_b(self):
        unit = Zone(self.config)

        unit.set_channel_b('chan1', '146.52', '146.52')
        self.assertEqual(unit.b_channel, 'chan1')
        self.assertEqual(unit.b_channel_rx, '146.52')
        self.assertEqual(unit.b_channel_tx, '146.52')

        unit.set_channel_b('eoc rpt', 444.000, 449.000)
        self.assertEqual(unit.b_channel, 'eoc rpt')
        self.assertEqual(unit.b_channel_rx, '444.0')
        self.assertEqual(unit.b_channel_tx, '449.0')

    def test_add_sort_simple(self):
        unit = Zone(self.config)
        unit.add_sort('a1', 'Fire Rpt')
        self.assertIsInstance(unit.sort_index, dict)
        self.assertEqual(unit.sort_index, {'a1': ['Fire Rpt']})

        unit.add_sort('b2', 'Local')
        print(unit.sort_index, file=sys.stderr)
        self.assertTrue('b2' in unit.sort_index.keys())
        self.assertTrue(['Local'] in unit.sort_index.values())

        unit.add_sort('a1', 'Another Rpt')
        print(unit.sort_index, file=sys.stderr)
        self.assertTrue('a1' in unit.sort_index.keys())
        self.assertTrue(['Fire Rpt', 'Another Rpt'] in unit.sort_index.values())

    def test_add_sort_dict(self):
        unit = Zone(self.config)
        unit.add('Home')
        unit.add_sort({'Home': 'a1'}, 'Fire Rpt')
        self.assertIsInstance(unit.sort_index, dict)
        self.assertEqual(unit.sort_index, {'a1': ['Fire Rpt']})

        # Added zone is not Travel
        unit.add_sort({'Travel': 'b2'}, 'Local')
        print(unit.sort_index, file=sys.stderr)
        self.assertFalse('b2' in unit.sort_index.keys())
        self.assertFalse(['Local'] in unit.sort_index.values())

        unit.add_sort({'Home': 'a1'}, 'Another Rpt')
        print(unit.sort_index, file=sys.stderr)
        self.assertTrue('a1' in unit.sort_index.keys())
        self.assertTrue(['Fire Rpt', 'Another Rpt'] in unit.sort_index.values())

    def test_choose_headers_with_v12(self):
        config = OpenStruct(self.config)
        config.fw_version = '1.2'

        unit = Zone(config)
        self.assertTrue("No." in Zone._headers)
        self.assertTrue("Zone Name" in Zone._headers)
        self.assertTrue("Zone Channel Member" in Zone._headers)
        self.assertTrue("Zone Channel Member RX Frequency" in Zone._headers)
        self.assertTrue("Zone Channel Member TX Frequency" in Zone._headers)
        self.assertTrue("A Channel" in Zone._headers)
        self.assertTrue("A Channel RX Frequency" in Zone._headers)
        self.assertTrue("A Channel TX Frequency" in Zone._headers)
        self.assertTrue("B Channel" in Zone._headers)
        self.assertTrue("B Channel RX Frequency" in Zone._headers)
        self.assertTrue("B Channel TX Frequency" in Zone._headers)
        self.assertFalse("Zone Hide" in Zone._headers)

    def test_choose_headers_with_v30(self):
        config = OpenStruct(self.config)
        config.fw_version = '3.0'

        unit = Zone(config)
        self.assertTrue("No." in Zone._headers)
        self.assertTrue("Zone Name" in Zone._headers)
        self.assertTrue("Zone Channel Member" in Zone._headers)
        self.assertTrue("Zone Channel Member RX Frequency" in Zone._headers)
        self.assertTrue("Zone Channel Member TX Frequency" in Zone._headers)
        self.assertTrue("A Channel" in Zone._headers)
        self.assertTrue("A Channel RX Frequency" in Zone._headers)
        self.assertTrue("A Channel TX Frequency" in Zone._headers)
        self.assertTrue("B Channel" in Zone._headers)
        self.assertTrue("B Channel RX Frequency" in Zone._headers)
        self.assertTrue("B Channel TX Frequency" in Zone._headers)
        self.assertTrue("Zone Hide" in Zone._headers)

    def test_render(self):
        unit = Zone(self.config)
        unit.add('Home')
        unit.add_member('freq1', '100.30', '200.60')
        unit.add_member('freq2', 444.123, 448.987)
        unit.commit()

        unit = Zone(self.config)
        unit.add('Zone02')
        unit.add_member('first', '123', '456')
        unit.add_sort('a2', 'first')
        unit.add_member('second', '789', '321')
        unit.add_sort('a1', 'second')
        unit.add_member('unsorted', '654', '987')
        unit.commit()

        output = Zone(self.config).render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == Zone._headers)
        self.assertTrue(output[1][1] == 'Home')
        self.assertTrue(re.search(r'freq1', output[1][2]))
        self.assertTrue(re.search(r'freq2', output[1][2]))
        self.assertTrue(re.search(r'100.30', output[1][3]))
        self.assertTrue(re.search(r'200.60', output[1][4]))
        self.assertTrue(re.search(r'444.123', output[1][3]))
        self.assertTrue(re.search(r'448.987', output[1][4]))

        self.assertTrue(output[2][1] == 'Zone02')
        self.assertTrue(output[2][2] == 'second|first|unsorted')
        self.assertTrue(output[2][3] == '789|123|654')
        self.assertTrue(output[2][4] == '321|456|987')


if __name__ == '__main__':
    unittest.main()
