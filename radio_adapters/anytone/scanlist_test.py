#!/usr/bin/env python3
import sys
import re
import unittest

from .scanlist import ScanList, ScanLists, csv_labels, _index
from lib.ostruct import OpenStruct

class TestScanList(unittest.TestCase):

    def tearDown(self):
        ScanLists.clear()
        _index.clear()

    def test_init_obj(self):
        unit = ScanList()
        self.assertEqual(unit.name, None)
        self.assertEqual(unit.members, list())
        self.assertEqual(unit.members_rx, list())
        self.assertEqual(unit.members_tx, list())
        self.assertEqual(unit.a_channel, None)
        self.assertEqual(unit.a_channel_rx, None)
        self.assertEqual(unit.a_channel_tx, None)
        self.assertEqual(unit.b_channel, None)
        self.assertEqual(unit.b_channel_rx, None)
        self.assertEqual(unit.b_channel_tx, None)

    def test_add(self):
        unit = ScanList()
        unit.add('Home')
        self.assertEqual(unit.name, 'Home')

    def test_add_member(self):
        unit = ScanList()
        unit.add_member('freq1', '100.30', '200.60')
        unit.add_member('freq2', 444.123, 448.987)

        self.assertTrue('freq1' in unit.members)
        self.assertTrue('freq2' in unit.members)
        self.assertTrue('100.30' in unit.members_rx)
        self.assertTrue('200.60' in unit.members_tx)
        self.assertTrue('444.123' in unit.members_rx)
        self.assertTrue('448.987' in unit.members_tx)

    def test_set_channel_a(self):
        unit = ScanList()

        unit.set_channel_a('chan1', '146.52', '146.52')
        self.assertEqual(unit.a_channel, 'chan1')
        self.assertEqual(unit.a_channel_rx, '146.52')
        self.assertEqual(unit.a_channel_tx, '146.52')

        unit.set_channel_a('eoc rpt', 444.000, 449.000)
        self.assertEqual(unit.a_channel, 'eoc rpt')
        self.assertEqual(unit.a_channel_rx, '444.0')
        self.assertEqual(unit.a_channel_tx, '449.0')

    def test_set_channel_b(self):
        unit = ScanList()

        unit.set_channel_b('chan1', '146.52', '146.52')
        self.assertEqual(unit.b_channel, 'chan1')
        self.assertEqual(unit.b_channel_rx, '146.52')
        self.assertEqual(unit.b_channel_tx, '146.52')

        unit.set_channel_b('eoc rpt', 444.000, 449.000)
        self.assertEqual(unit.b_channel, 'eoc rpt')
        self.assertEqual(unit.b_channel_rx, '444.0')
        self.assertEqual(unit.b_channel_tx, '449.0')

    def test_render(self):
        unit = ScanList()
        unit.add('Home')
        unit.add_member('freq1', '100.30', '200.60')
        unit.add_member('freq2', 444.123, 448.987)
        unit.commit()

        unit = ScanList()
        unit.add('ScanList02')
        unit.add_member('first', '123', '456')
        unit.add_member('second', '789', '321')
        unit.add_member('unsorted', '654', '987')
        unit.commit()

        output = ScanList().render()
        print(output, file=sys.stderr)
        self.assertTrue(output[0] == csv_labels)
        self.assertTrue(output[1][1] == 'Home')
        self.assertTrue(re.search(r'freq1', output[1][2]))
        self.assertTrue(re.search(r'freq2', output[1][2]))
        self.assertTrue(re.search(r'100.30', output[1][3]))
        self.assertTrue(re.search(r'200.60', output[1][4]))
        self.assertTrue(re.search(r'444.123', output[1][3]))
        self.assertTrue(re.search(r'448.987', output[1][4]))

        self.assertTrue(output[2][1] == 'ScanList02')
        self.assertTrue(output[2][2] == 'first|second|unsorted')
        self.assertTrue(output[2][3] == '123|789|654')
        self.assertTrue(output[2][4] == '456|321|987')


if __name__ == '__main__':
    unittest.main()
