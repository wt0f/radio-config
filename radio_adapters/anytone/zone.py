
import logging

from lib.ostruct import OpenStruct

from pprint import pprint

Zones = list()

class Zone:
    csv_labels = ({
                'attr': 'num',
                'header': 'No.'
              }, {
                'attr': 'name',
                'header': 'Zone Name'
              }, {
                'attr': 'members',
                'header': 'Zone Channel Member'
              }, {
                'attr': 'members_rx',
                'header': 'Zone Channel Member RX Frequency'
              }, {
                'attr': 'members_tx',
                'header': 'Zone Channel Member TX Frequency'
              }, {
                'attr': 'a_channel',
                'header': 'A Channel'
              }, {
                'attr': 'a_channel_rx',
                'header': 'A Channel RX Frequency'
              }, {
                'attr': 'a_channel_tx',
                'header': 'A Channel TX Frequency'
              }, {
                'attr': 'b_channel',
                'header': 'B Channel'
              }, {
                'attr': 'b_channel_rx',
                'header': 'B Channel RX Frequency'
              }, {
                'attr': 'b_channel_tx',
                'header': 'B Channel TX Frequency'
              }, {
                'attr': 'zone_hide',
                'header': 'Zone Hide',
                'selector': '>3.0'
              })

    _headers = list()
    _attrs = list()
    _index = list()

    def __init__(self, config):
        self.name         = None
        self.members      = []
        self.members_rx   = {}
        self.members_tx   = {}
        self.a_channel    = None
        self.a_channel_rx = None
        self.a_channel_tx = None
        self.b_channel    = None
        self.b_channel_rx = None
        self.b_channel_tx = None
        self.zone_hide    = False

        self.sort_index   = {}
        self.choose_headers(config)

    def add(self, name):
        self.name = name

    def add_member(self, name, rx, tx):
        self.members.append(name)
        self.members_rx[name] = str(rx)
        self.members_tx[name] = str(tx)

    def add_sort(self, key, chan_name):
        sort_key = key
        if type(key) == dict:
            if self.name in key:
                sort_key = key[self.name]
            else:
                return

        if sort_key in self.sort_index:
            self.sort_index[sort_key].append(chan_name)
        else:
            self.sort_index[sort_key] = [chan_name]

    def set_channel_a(self, name, rx, tx):
        self.a_channel = name
        self.a_channel_rx = str(rx)
        self.a_channel_tx = str(tx)

    def set_channel_b(self, name, rx, tx):
        self.b_channel = name
        self.b_channel_rx = str(rx)
        self.b_channel_tx = str(tx)

    def commit(self):
        if self.name not in Zone._index:
            Zone._index.append(self.name)

            record = {'name': self.name,
                      'members': self.members,
                      'members_rx': self.members_rx,
                      'members_tx': self.members_tx,
                      'a_channel': self.a_channel,
                      'a_channel_rx': self.a_channel_rx,
                      'a_channel_tx': self.a_channel_tx,
                      'b_channel': self.b_channel,
                      'b_channel_rx': self.b_channel_rx,
                      'b_channel_tx': self.b_channel_tx,
                      'zone_hide': self.zone_hide,
                      'sort_index': self.sort_index,
                      }
            Zones.append(OpenStruct(record))

    def choose_headers(self, config):
        if len(Zone._attrs) == 0:
            fw_version = config.fw_version
            headers = list()

            for definition in Zone.csv_labels:
                if 'selector' in definition.keys():
                    if definition['selector'].startswith('>'):
                        min_vers = float(definition['selector'][1:])
                        if float(config.fw_version) >= min_vers:
                            Zone._headers.append(definition['header'])
                            Zone._attrs.append(definition)
                    elif definition['selector'].startswith('<'):
                        max_vers = float(definition['selector'][1:])
                        if float(config.fw_version) < max_vers:
                            Zone._headers.append(definition['header'])
                            Zone._attrs.append(definition)
                    elif definition['selector'].startswith('='):
                        vers = float(definition['selector'][1:])
                        if float(config.fw_version) == vers:
                            Zone._headers.append(definition['header'])
                            Zone._attrs.append(definition)
                    else:
                        raise RuntimeError(f"Bad selector in Zones cvs_labels definitons: {definition['selector'][0]}")
                else:
                    # No selector means "all"
                    Zone._headers.append(definition['header'])
                Zone._attrs.append(definition)

    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return Zone.name(args)
        return method

    @staticmethod
    def render():
        # index all memories to produce ordering information

        count = 1
        output = [ Zone._headers ]
        for entry in Zones:
            logging.info("Populating zone: {}".format(entry.name))
            zone_members = list(entry.members)
            members = []
            members_rx = []
            members_tx = []

            # sort the members with the existing sort_index
            mem_order = list(entry['sort_index'].keys())
            mem_order.sort()

            for order in mem_order:
                for name in entry.sort_index[order]:
                    if name not in members:
                        logging.debug('Adding {} with sort key {}'.format(name, order))
                        members.append(name)
                        members_rx.append(entry.members_rx[name])
                        members_tx.append(entry.members_tx[name])
                        zone_members = list(filter((name).__ne__, zone_members))

            # add the remaining members
            for name in zone_members:
                logging.debug('Adding {} without sort key'.format(name))
                members.append(name)
                members_rx.append(entry.members_rx[name])
                members_tx.append(entry.members_tx[name])

            datum = []
            for attr in Zone._attrs:
                if attr['attr'] == "num":
                    datum.append(count)
                elif attr['attr'].startswith("members"):
                    datum.append("|".join(locals()[attr['attr']]))
                else:
                    datum.append(entry[attr['attr']])
            output.append(datum)
            count += 1

        return output
