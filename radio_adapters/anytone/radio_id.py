
from lib.ostruct import OpenStruct


RadioIDs = list()

csv_labels = ("No.",
              "Radio ID",
              "Name")


class RadioID:

    def __init__(self):
        self.name = None
        self.id   = None

    def add(self, name, id):
        self.name = name
        self.id = id
        RadioIDs.append(OpenStruct({'name': self.name, 'id': self.id}))

    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return RadioID.name(args)
        return method

    @staticmethod
    def findByName(name):
        for entry in RadioIDs:
            if entry['name'] == name:
                return entry


    @staticmethod
    def render():
        count = 1
        output = [csv_labels]
        for entry in RadioIDs:
            output.append((count, entry.id, entry.name))
            count += 1

        return output
