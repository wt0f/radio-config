
from lib.ostruct import OpenStruct

GroupLists = list()

csv_labels = ("No.",
              "Group Name",
              "Contact",
              "Contact TG/DMR ID")

_index = []

class GroupList:

    def __init__(self):
        self.name         = None
        self.members      = []
        self.members_tg   = []

    def add(self, name):
        self.name = name

    def add_member(self, name, tg):
        self.members.append(name)
        self.members_tg.append(str(tg))

    def commit(self):
        if self.name not in _index:
            _index.append(self.name)

            record = {'name': self.name, 'members': self.members,
                      'members_tg': self.members_tg}
            GroupLists.append(OpenStruct(record))

    # defining __getattr__ should allow us to call static methods without
    # having to define them in the object itself
    def __getattr__(self, name):
        def method(*args):
            return GroupList.name(args)
        return method

    @staticmethod
    def render():
        count = 1
        output = [csv_labels]
        for entry in GroupLists:
            output.append((count, entry.name, "|".join(entry.members),
                           "|".join(entry.members_tg)))
            count += 1

        return output
