

class DupError(RuntimeError):
    pass

class NotFoundError(RuntimeError):
    pass
