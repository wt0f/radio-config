
from codeplug_data.base import Base


class Stations(Base):
    defaults = {'dstar_cs'       : 'CQCQCQ',
                'dstar_my'       : '%radio_ids#dstar&default',
                'fm_sql_mode'    : 'none',
                'fm_dcs_polarity': 'NN',
                'fm_bandwidth'   : 'wide',
                'dmr_cc'         : '1',
                'dmr_radio_id'   : '%radio_ids#dmr&default',
                'dmr_mode'       : 'repeater',
                'dmr_tx_allow'   : 'same_cc',
                'power'          : 'low'}
    data_type = 'station'
    key_name = 'name'

    def remove_station(self, name):
        # remove the station from the list of stations
        del(self.data[name])
        # remove the station reference in the tag data
        for tag in self.tags:
            if name in self.tags[tag]:
                self.tags[tag].remove(name)