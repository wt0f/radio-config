#!/usr/bin/env python3

import sys
import pytest

from radio_ids import RadioIDs
from exceptions import DupError

class TestRadioIDs:

    testdata = [
        {
            'tags': ['all'],
            'data': {'id': '1234', 'name': 'WT0F'}
        },
        {
            'tags': ['home', 'travel'],
            'data': {'id': '6647', 'name': 'W1AW'}
        },
        {
            'tags': ['home'],
            'data': {'id': '8329', 'name': 'K1ABC'}
        }
    ]

    @pytest.fixture
    def empty_data(self):
        unit = RadioIDs.instance()
        yield unit
        unit.data.clear()
        unit.tags.clear()

    @pytest.fixture
    def load_data(self, empty_data):
        unit = empty_data
        for datum in self.testdata:
            unit.add(datum['data'], datum['tags'])

        return unit

    def test_init(self):
        with pytest.raises(RuntimeError):
            RadioIDs()

    def test_defaults(self, empty_data):
        unit = empty_data
        assert unit.defaults == dict()
        assert unit.data_type == "radioid"
        assert unit.key_name == 'id'

