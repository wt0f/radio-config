import logging
import re
import copy

from codeplug_data.exceptions import DupError


class Base:
    _instance = None

    def __init__(self):
        raise RuntimeError('Call instance() method instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)
            # further initialization
            cls._instance.data = dict()
            cls._instance.tags = dict()
        return cls._instance

    def add(self, new_data={}, tags=[]):
        logging.debug("%s.add(%s, %s)" % (self.__class__, new_data, tags))

        if type(tags) == str:
            tags = [ tags ]

        key = new_data[self.key_name]
        if key in self.data:
            logging.error(f"Multiple object ({key})) in {self.data_type} definitions. {new_data=}")
            logging.error(f"Original object ({key}) {self.data[key]=}")
            raise DupError(f"Multiple object ({key}) in {self.data_type} definitions")

        datum = self.defaults.copy()
        datum.update(copy.deepcopy(new_data))
        self.data[key] = datum.copy()
        for tag in tags:
            if tag in self.tags.keys():
                logging.debug(f"Appending {key} to {tag} tag")
                self.tags[tag].append(key)
            else:
                logging.debug(f"Creating {tag} tag with {key}")
                self.tags[tag] = [key]

        # updateIndexes() allow different model components to manage their
        # indexes when adding new data
        self.updateIndexes(datum)

    # updateIndexes() is stubbed out for component use
    def updateIndexes(self, new_data):
        return

    def select_tag(self, tag):
        if tag in self.tags.keys():
            ids = self.tags[tag]
            data = [self.data[id] for id in ids]
            return data
        return []

    def query(self, terms):
        ids = None

        for term in terms.split('&'):
            term = term.strip()
            if term.startswith('-'):
                # remove IDs from accumalated list
                ids.difference_update(self.tags[term[1:]])
            else:
                # Perform an intersection of existing terms
                if term in self.tags.keys():
                    canidates = set(self.tags[term])
                    if ids:
                        ids = ids - (ids - canidates)
                    else:
                        ids = canidates
                    # ids.update(self.tags[term])
        logging.debug("Results from query: {} = {}".format(terms, ids))
        if ids:
            return [self.data[s] for s in ids]
        return []

    def dump(self):
        return {'data': self.data, 'tags': self.tags}

