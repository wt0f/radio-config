#!/usr/bin/env python3

import sys
import pytest

from stations import Stations
from exceptions import DupError
from pprint import pprint
class TestStations:

    testdata = [
        {
            'tags': ['all'],
            'data': {'name': 'A Rpt', 'rx': 12, 'tx': 34}
        },
        {
            'tags': ['home', 'travel'],
            'data': {'name': 'B Rpt', 'rx': 56, 'tx': 78}
        },
        {
            'tags': ['home'],
            'data': {'name': 'C Rpt', 'rx': 90, 'tx': 10}
        }
    ]

    @pytest.fixture
    def empty_data(self):
        unit = Stations.instance()
        yield unit
        unit.data.clear()
        unit.tags.clear()

    @pytest.fixture
    def load_data(self, empty_data):
        unit = empty_data
        for datum in self.testdata:
            unit.add(datum['data'], datum['tags'])

        return unit

    def test_init(self):
        with pytest.raises(RuntimeError):
            Stations()

    def test_defaults(self, empty_data):
        unit = empty_data
        expected_defaults = {'dstar_cs'       : 'CQCQCQ',
                             'dstar_my'       : '%radio_ids#dstar&default',
                             'fm_sql_mode'    : 'none',
                             'fm_dcs_polarity': 'NN',
                             'fm_bandwidth'   : 'wide',
                             'dmr_cc'         : '1',
                             'dmr_radio_id'   : '%radio_ids#dmr&default',
                             'dmr_mode'       : 'repeater',
                             'dmr_tx_allow'   : 'same_cc',
                             'power'          : 'low'}

        for key in expected_defaults.keys():
            assert unit.defaults[key] == expected_defaults[key]
        assert unit.data_type == "station"
        assert unit.key_name == 'name'

    def test_remove_station(self, load_data):
        unit = load_data
        unit.remove_station('B Rpt')
        assert 'B Rpt' not in unit.data
        for tag in unit.tags:
            assert 'B Rpt' not in unit.tags[tag]
