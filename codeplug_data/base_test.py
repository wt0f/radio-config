#!/usr/bin/env python3

import sys
import pytest

from base import Base
from exceptions import DupError

class TestBase:

    testdata = [
        {
            'tags': ['all'],
            'data': {'id': '1234', 'name': 'WT0F'}
        },
        {
            'tags': ['home', 'travel'],
            'data': {'id': '6647', 'name': 'W1AW'}
        },
        {
            'tags': ['home'],
            'data': {'id': '8329', 'name': 'K1ABC'}
        }
    ]

    @pytest.fixture
    def empty_obj(self):
        unit = Base.instance()

        # Force base to "emulate" a model component
        unit.key_name = 'name'
        unit.data_type = 'base'
        unit.defaults = dict()
        yield unit
        unit.data.clear()
        unit.tags.clear()

    @pytest.fixture
    def load_data(self, empty_obj):
        unit = empty_obj

        for datum in self.testdata:
            unit.add(datum['data'], datum['tags'])
        return unit

    def test_init(self):
        with pytest.raises(RuntimeError):
            Base()

    def test_instance(self, empty_obj):
        unit = empty_obj
        assert unit.data == dict()
        assert unit.tags == dict()

    def test_instance_is_singleton(self, empty_obj):
        unit = empty_obj
        assert unit is Base.instance()

    def test_add(self, empty_obj):
        unit = empty_obj
        unit.add({'id': '1234', 'name': 'WT0F'}, ['all'])
        assert unit.tags['all'] == ['WT0F']
        assert unit.data == { 'WT0F': {'id': '1234', 'name': 'WT0F'}}

    def test_add_with_defaults(self, empty_obj):
        unit = empty_obj
        unit.defaults = {'attr1': 'val1', 'attr2': 'val2'}
        unit.add({'id': '1234', 'name': 'WT0F'}, ['all'])
        assert unit.data == { 'WT0F': {'id': '1234', 'name': 'WT0F', 'attr1': 'val1', 'attr2': 'val2'}}

    def test_select_tag(self, load_data):
        unit = load_data
        results = unit.select_tag('home')
        expected = [{'id': '6647', 'name': 'W1AW'},
                    {'id': '8329', 'name': 'K1ABC'}]
        print(results, file=sys.stderr)
        if results == expected:
            assert True
        else:
            expected.append(expected.pop(0))
            if results == expected:
                assert True
            else:
                assert False

        results = unit.select_tag('emcomm')
        print(results, file=sys.stderr)
        assert len(results) == 0

    def test_query(self, load_data):
        unit = load_data
        results = unit.query('travel')
        expected = [{'id': '6647', 'name': 'W1AW'}]
        assert results == expected

        results = unit.query('all&travel')
        expected = []
        if results == expected:
            assert True
        else:
            expected.append(expected.pop(0))
            if results == expected:
                assert True
            else:
                assert False

        results = unit.query('home&-travel')
        expected = [{'id': '8329', 'name': 'K1ABC'}]
        assert results == expected

        results = unit.query('home&travel')
        expected = [{'id': '6647', 'name': 'W1AW'}]
        assert results == expected
