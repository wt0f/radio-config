
from codeplug_data.base import Base

class Grouplists(Base):
    defaults = {}
    data_type = 'grouplist'
    key_name = 'name'
