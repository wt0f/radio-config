import re

def pad_numerics(val, padding=5):
    # pad all numeric values in val
    val = re.sub(r'(\d+)',
        lambda m: '{}{}'.format('0'*(padding - len(m.group(1))), m.group(1)),
        val, re.S)
    return val


def canonicalize_seq(node_data):
    if type(node_data) == str:
        node_data = pad_numerics(node_data)
    elif type(node_data) == int:
        node_data = pad_numerics(str(node_data))
    elif type(node_data) == dict:
        for zone in node_data.keys():
            node_data[zone] = pad_numerics(str(node_data[zone]))
    else:
        print('Unsupported sequence data type: {}'.format(type(node_data)))
        node_data = None
    return node_data


