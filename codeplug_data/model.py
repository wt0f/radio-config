import logging
import yaml
import os
import sys

# from codeplug_data.exceptions import DupError
from .radio_ids import RadioIDs
from .stations import Stations
from .talkgroups import Talkgroups
from .scanlists import Scanlists
from .grouplists import Grouplists
from .util import *

from pprint import pprint,pformat
import pdb

class Model:
    _instance = None

    def __init__(self):
        raise RuntimeError('Call instance() method instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)

            # Initialize component object to store raw codeplug data
            cls._instance.radio_ids = RadioIDs.instance()
            cls._instance.stations = Stations.instance()
            cls._instance.talkgroups = Talkgroups.instance()
            cls._instance.grouplists = Grouplists.instance()
            cls._instance.scanlists = Scanlists.instance()

        return cls._instance

    def read_data_files(self, options):
        data_dir = options.input
        sections = options.sections or ['stations', 'talkgroups', 'scanlists', 'grouplists']

        logging.debug("read_data_files(%s)" % data_dir)
        logging.info("Reading files from %s to build code plug data"
                     % data_dir)

        # Consolidate all the raw data for processing
        data = {"stations": [], "talkgroups": [], "dmr": {},
                "scanlists": [], "grouplists": []}
        for root, dirs, files in os.walk(data_dir):
            logging.debug("root  = %s" % root)
            logging.debug("dirs  = %s" % dirs)
            logging.debug("files = %s" % files)

            for datafile in files:
                path = "%s/%s" % (root, datafile)
                logging.info("Reading %s" % path)
                with open(path, 'r') as section:
                    logging.debug("Processing %s" % path)
                    try:
                        new_data = yaml.load(section, Loader=yaml.SafeLoader)
                    except yaml.composer.ComposerError as e:
                        logging.critical(f"{e} in {path}")
                        sys.exit(1)

                    # Tag each item in each section with the src file
                    for sect in new_data.keys():
                        if sect in sections:
                            for i in range(len(new_data[sect])):
                                logging.debug(f"Adding _src_file attr to {new_data[sect][i]=}")
                                new_data[sect][i]['_src_file'] = path

                    data = Model.merge_codeplug_data(data, new_data)

        # Sort the raw data into compoent objects
        try:
            if 'dmr' in data:
                if 'radio_ids' in data['dmr']:
                    for entry in data['dmr']['radio_ids']:
                        tags = 'tags' in entry and entry['tags'] or []
                        self.radio_ids.add(new_data=entry, tags=tags)
        except KeyError as e:
            logging.critical(f"{e} for entry {entry=}")

        try:
            if 'talkgroups' in data and 'talkgroups' in sections:
                for entry in data['talkgroups']:
                    tags = 'tags' in entry and entry['tags'] or []
                    self.talkgroups.add(new_data=entry, tags=tags)
        except KeyError as e:
            logging.critical(f"{e} for entry {entry=}")

        try:
            if 'grouplists' in data and 'grouplists' in sections:
                for entry in data['grouplists']:
                    tags = 'tags' in entry and entry['tags'] or []
                    self.grouplists.add(new_data=entry, tags=tags)
        except KeyError as e:
            logging.critical(f"{e} for entry {entry=}")

        try:
            if 'scanlists' in data and 'scanlists' in sections:
                for entry in data['scanlists']:
                    tags = 'tags' in entry and entry['tags'] or []
                    self.scanlists.add(new_data=entry, tags=tags)
        except KeyError as e:
            logging.critical(f"{e} for entry {entry=}")

        if 'stations' in data and 'stations' in sections:
            for entry in data['stations']:
                sta_data = {}
                tags = 'tags' in entry and entry['tags'] or []

                # name is a required field so we will break if not specifed
                sta_data['name'] = entry['name']

                # bring in the rest of the standard fields
                for attr in entry.keys():
                    if attr in ('rx', 'tx', 'power', 'scanlist', 'chan_type',
                                'tx_inhibit', ):
                        sta_data[attr] = entry[attr]

                sta_data['name16'] = 'name16' in entry and entry['name16'] \
                    or sta_data['name'][0:16]
                sta_data['name12'] = 'name12' in entry and entry['name12'] \
                    or sta_data['name16'][0:12]
                sta_data['name8'] = 'name8' in entry and entry['name8'] \
                    or sta_data['name12'][0:8]
                sta_data['name6'] = 'name6' in entry and entry['name6'] \
                    or sta_data['name8'][0:6]

                # process and store the sequence value
                if 'seq' in entry:
                    sta_data['sequence'] = canonicalize_seq(entry['seq'])

                for modulation in ['fm', 'dmr', 'dstar', 'fusion', 'p25']:
                    if modulation in entry:
                        sta_data['chan_mode'] = modulation
                        for item in entry[modulation]:
                            # special case for DSTAR callsign memories
                            if modulation == 'dstar' and item in ['mycall', 'rpt1', 'rpt2', 'cs']:
                                callsign = None
                                if entry[modulation][item]:
                                    callsign = entry[modulation][item].translate({'.': ' '})
                                sta_data[modulation+'_'+item] = callsign
                            else:
                                sta_data[modulation+'_'+item] = entry[modulation][item]

                # adjust for shortcut settings
                if 'fm_tone' in sta_data:
                    sta_data['fm_rx_tone'] = sta_data['fm_tx_tone'] = sta_data['fm_tone']
                if 'fm_dcs_code' in sta_data:
                    sta_data['fm_rx_dcs_code'] = sta_data['fm_tx_dcs_code'] = sta_data['fm_dcs_code']
                if 'fm_dcs_polarity' in sta_data:
                    sta_data['fm_rx_dcs_polarity'] = sta_data['fm_dcs_polarity'][0]
                    sta_data['fm_tx_dcs_polarity'] = sta_data['fm_dcs_polarity'][1]
                if 'freq' in entry:
                    sta_data['rx'] = sta_data['tx'] = entry['freq']

                self.stations.add(new_data=sta_data, tags=tags)
        return self

    @staticmethod
    def merge_codeplug_data(orig, new):
        # TODO: None of this checks for duplicate entries
        # TODO: Need schema validation
        for k,v in orig.items():
            if isinstance(v, dict):
                orig[k].update(new.setdefault(k, {}))
            elif isinstance(v, list):
                if k in new.keys():
                    for v2 in new[k]:
                        orig[k].append(v2)
            else:
                # Add a new key
                orig[k] = v
        return orig

