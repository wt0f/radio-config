#!/usr/bin/env python3

import sys
import pytest

from scanlists import Scanlists
from exceptions import DupError

class TestScanlists:

    testdata = [
        {
            'tags': ['all'],
            'data': {'name': 'All', 'members': ['a', 'b', 'c']}
        },
        {
            'tags': ['home', 'travel'],
            'data': {'name': 'Seattle', 'members': ['b', 'd', 'e']}
        },
        {
            'tags': ['home'],
            'data': {'name': 'Home', 'members': ['x', 'y', 'z']}
        }
    ]

    @pytest.fixture
    def empty_data(self):
        unit = Scanlists.instance()
        yield unit
        unit.data.clear()
        unit.tags.clear()

    @pytest.fixture
    def load_data(self, empty_data):
        unit = empty_data
        for datum in self.testdata:
            unit.add(datum['data'], datum['tags'])

        return unit

    def test_init(self):
        with pytest.raises(RuntimeError):
            Scanlists()

