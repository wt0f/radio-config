#!/usr/bin/env python3

import sys
import pytest

from talkgroups import Talkgroups
from exceptions import DupError

class TestTalkgroups:

    testdata = [
        {
            'tags': ['all'],
            'data': {'name': 'Simplex', 'talkgroup': 99, 'calltype': 'group'}
        },
        {
            'tags': ['home', 'travel'],
            'data': {'name': 'TGIF', 'talkgroup': 5031665}
        },
        {
            'tags': ['home'],
            'data': {'name': 'TGIF Disconnect', 'talkgroup': 5004000, 'calltype': 'private'}
        }
    ]

    @pytest.fixture
    def empty_data(self):
        unit = Talkgroups.instance()
        yield unit
        unit.data.clear()
        unit.tags.clear()

    @pytest.fixture
    def load_data(self, empty_data):
        unit = empty_data
        for datum in self.testdata:
            unit.add(datum['data'], datum['tags'])

        return unit

    def test_init(self):
        with pytest.raises(RuntimeError):
            Talkgroups()

    def test_defaults(self, empty_data):
        unit = empty_data
        assert unit.defaults == {'calltype': 'group'}
        assert unit.data_type == "talkgroup"
        assert unit.key_name == 'name'

    def test_findByName(self, load_data):
        unit = load_data
        assert unit.findByName('TGIF')['talkgroup'] == self.testdata[1]['data']['talkgroup']

    def test_findByID(self, load_data):
        unit = load_data
