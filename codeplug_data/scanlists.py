
from codeplug_data.base import Base


class Scanlists(Base):
    defaults = {}
    data_type = 'scanlist'
    key_name = 'name'

    def remove_station(self, name):
        # scan the list of scanlists and remove any reference to the station
        for scanlist in self.data.keys():
            if name in self.data[scanlist]['members']:
                self.data[scanlist]['members'].remove(name)
