#!/usr/bin/env python3

import sys
import pytest

from grouplists import Grouplists
from exceptions import DupError

class TestGrouplists:

    testdata = [
        {
            'tags': ['all'],
            'data': {'name': 'All Rpts', 'members': ['a', 'b', 'c']}
        },
        {
            'tags': ['home', 'travel'],
            'data': {'name': 'WA Rpts', 'members': ['d', 'e', 'f']}
        },
        {
            'tags': ['home'],
            'data': {'name': 'Local Rpts', 'members': ['x', 'y', 'z']}
        }
    ]

    @pytest.fixture
    def empty_data(self):
        unit = Grouplists.instance()
        yield unit
        unit.data.clear()
        unit.tags.clear()

    @pytest.fixture
    def load_data(self, empty_data):
        unit = empty_data
        for datum in self.testdata:
            unit.add(datum['data'], datum['tags'])

        return unit

    def test_init(self):
        with pytest.raises(RuntimeError):
            Grouplists()

    def test_defaults(self, empty_data):
        unit = empty_data
        assert unit.defaults == dict()
        assert unit.data_type =="grouplist"
        assert unit.key_name == 'name'

