
from codeplug_data.base import Base


class RadioIDs(Base):
    defaults = {}
    data_type = 'radioid'
    key_name = 'id'
