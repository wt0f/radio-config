import logging
import copy

from codeplug_data.base import Base


class Talkgroups(Base):
    defaults = {'calltype': 'group'}
    data_type = 'talkgroup'
    key_name = 'name'
    tg_usage = dict()

    def updateIndexes(self, new_data):
        name = new_data[self.key_name]
        if name in self.tg_usage:
            self.tg_usage[name] = self.tg_usage[name] + 1
        else:
            self.tg_usage[name] = 1

    def findByName(self, name):
        return self.data[name]

