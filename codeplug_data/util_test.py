#!/usr/bin/env python3

import sys
import pytest

from util import pad_numerics, canonicalize_seq

class TestUtil:

    def test_pad_numerics(self):
        testcases = [
            ('abc', 'abc', 5),
            ('a1', 'a00001', 5),
            ('a1', 'a00001', None),
            ('a3b8', 'a003b008', 3),
            ('a3b839', 'a003b839', 3),
            ('a3b1200', 'a003b1200', 3),
            ('a4', 'a000000000004', 12),
            ('a004', 'a00004', None)
        ]

        for case in testcases:
            print("pad_numerics({})".format(case[0]), file=sys.stderr)
            if case[2]:
                assert pad_numerics(case[0], case[2]) == case[1]
            else:
                assert pad_numerics(case[0]) == case[1]

    def test_canonicalize_seq(self):
        testcases = [
            ('a', 'a'),
            ('a2', 'a00002'),
            ('a1b2c3', 'a00001b00002c00003'),
            ('14', '00014'),
            (14, '00014'),
        ]

        for case in testcases:
            print("canonicalize_seq({})".format(case[0]), file=sys.stderr)
            assert canonicalize_seq(case[0]) == case[1]

    def test_canonicalize_seq_dict(self):
        testcases = [
            ({'a': 'a123', 'b3': 'a4b8', 'c': 123}, {'a': 'a00123', 'b3': 'a00004b00008', 'c': '00123'})
        ]

        for case in testcases:
            print("canonicalize_seq({})".format(case[0]), file=sys.stderr)
            result = canonicalize_seq(case[0])

            for key in case[1].keys():
                print("testing {} key".format(key), file=sys.stderr)
                assert key in result.keys()
                assert result[key] == case[1][key]
