Schemas
=======

Stations
--------

The list of stations are introduced by the `stations` YAML dictionary. Multiple
files can contain station information as long as in each file the station
information is listed under the `stations:` key.

```yaml
stations:
    - name: # Full name
      name12: # Name limited to 12 characters
      name8: # Name limited to 8 characters
      name6: # Name limited to 6 characters
      rx: # Frequency in MHz
      tx: # Frequency in MHz
      freq:
      tx_inhibit: # on, off, true, false
      power: # TX power level: slo, low, med, high, turbo
      scanlist:
      chan_type: # analog, digital, analog_tx, digital_tx
      seq: # field to provide ordering of stations in a zone
      fm:
        rx_tone:
        tx_tone:
        tone: # CSSTS tone
        rx_dcs_code:
        tx_dcs_code:
        dcs_code: # DCS code with leading 0
        dcs_polarity: # NN, RR, RN, NR
        rx_dcs_polarity: # N, R
        tx_dcs_polarity: # N, R
        sql_mode: # Squelch type: none, tone, tsql, dcs, off
        bandwidth: # narrow, wide, 25K, 12.5K
      dmr:
        slot:
        cc: # DMR color code
        tx_allow: # always, free, same_cc, different_cc
        talkgroup: # Talkgroup name
        radio_id: # RadioID name
        rx_group_list:
        sms_forbit: no
        data_act_forbid: no
        mode: # DMR mode: simplex, repeater, dual_slot
      dstar:
        cs: # CQCQCQCQ
        rpt1: # WA7FW  C
        rpt2: # WA7FW  G
        my: # WT0F   P
        sql_mode:
      tags:
        - # list of tags or this entry
```

Only one of the `fm`, `dmr`, or `dstar` keys can be specified. `chan_type`
allows the radios that are capable to receive both analog / digital and
specify which mode to transmit with. `chan_type` can also be used to
override the mode in special circumstances.

The `seq` field is used to organize and order the stations in a zone.
It should be noted that the raw memories in the radio are written is a
disorganized format, but the zones will organize the stations in a
predefined order. The value for the `seq` field does not need to be
strictly numeric.

Any numeric components within the `seq` field will be replaced with zero
padded values (5 digits) so that the sorting of the `seq` fields will be
predictable. For example if the `seq` field is specified as "ab3h89" then
the `seq` field will be transformed and stored as "ab00003h00089". Thus
another `seq` field of "ab18h" will be sorted correctly and appear after
"ab3h89".

It is also possible to provide different sort values to different zones.
If the `seq` field is provided as a dictionary instead of a single value,
then multiple zones can be assigned differing sort values. The key of the
dictionary is the zone name and the value is the sort value.



Scan Lists
----------

The following is a few ideas on how to build a scanlist of literal
station names and tag expressions.

```yaml
scanlists:
    - name:
      members:
        - ICST 1
        - /icst
        - @geo:county:king&org:ares
        - %geo:city:seattle
        - tag=geo
      mode:
      tags:
        -

```


Receive Group Lists
-------------------

```yaml
grouplists:
    - name:
      members:
        -
      tags:
        -
```

