DMR Specific Schemas
====================

Talkgroups
----------

```yaml
talkgroups:
    - name: # Name
      talkgroup: # Talkgroup number
      calltype: # group or private
      tags:
        -
```

Radio IDs List
--------------

```yaml
dmr:
  radio_ids:
    - id:
      name:
      tags:
        -
```
