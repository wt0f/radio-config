Everything is tagged
====================

All items in the code plug files can be tagged. Using tags allows specific
items to be inserted into a code plug.

Selecting with tags
-------------------

Sections in the radio's configuration file (principally the `zone` section)
use a filter expression consisting of tags. The expression will collect
items that are tagged with all the tags in the expression. A typical
filter expression is constructed like:

    TAG1 & TAG2 & TAG3

Spaced in the expression are optional, but do make the expression more
readable. In this simple example only items that are tagged with `TAG1`,
`TAG2` and `TAG3` will be included in the section.

It is possible to remove items that have specific tags also. Because the
query engine processes tags from left to right, it is best to put the
tags that need to be excluded at the end of the filter expression. Otherwise
items with the negated tag may appear in the list of items. Negated
tags are prefixed with a `-` in the expression. For example:

    TAG1 & TAG2 & -TAG3

So a list of items that have both `TAG1` and `TAG2` is built and then
any item that is also tagged with `TAG3` would be dropped from the list.
