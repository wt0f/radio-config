File Structure
==============

The YAML files are broken up into a couple of categories. It does not matter
what you call the files (filenames are irrelevant and used just for your
convenience) and in fact all the YAML could be included into a single file.

The primary "sections" that get created are:

* stations
* talkgroups

Each radio has a configuration file created for it that then has the settings
for that radio and what information from the various sections get pulled
in for the generation of the code plug.

The configuration file will instruct the scripts how to generate the CSV files
to use with your CPS software to program each individual radio. So there is
still no way to escape using the CPS software.


Radio Configuration File
------------------------

Most radio configurations will use something like the following:

```yaml
---
configuration:
  name:
  radio_type: anytone
  version: 21-%nM

  settings:
    bands:
      - range: 136-174
      - range: 400-470
    repeater_offsets:
      2M: 0.600
      1.25M: 1.6
      70CM: 5.0
    dmr:
      radio_ids:
        tags:
          -
      talkgroups:
        tags:
          -
      receivegroups:
        tags:
          -
    dstar:
      mycall:
    aprs:
      mycall: WT0F-2
      path: WIDE1-1,WIDE2-2
    scanlists:
      tags:
        -
  zones:
    - name:
      tags:
        -

  scanlists:
    - name:
      tags:
        -
```

Chirp uses a simpler radio config:

```yaml
configuration:
  name: baofeng-standard
  radio_type: chirp
  version: 21-%nM

  settings: {}

  memories:
    - rule1
    - rule2

```



