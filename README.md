# Radio Configurator

[![pipeline status](https://gitlab.com/wt0f/radio-config/badges/master/pipeline.svg)](https://gitlab.com/wt0f/radio-config/-/commits/master)
[![coverage report](https://gitlab.com/wt0f/radio-config/badges/master/coverage.svg)](https://gitlab.com/wt0f/radio-config/-/commits/master)

Describe your code plug through YAML files.

Full documentation describing the codeplug schema and radio configuration
file format is located at https://wt0f.gitlab.io/radio-config/. This
documentation is automatically generated from documentation within this
repository rather than try to duplicate critical information in this README.

## But Why?

If you are like me, you have several different radios that you use--sometimes
from different manufactures--that have pretty much the same programming. But
there are some differences. Here are a few things that has caused me to
manage my radio configurations as YAML files.

* Most of my radios have effectively the same code plug, but there is a bit
  of variation in the code plugs. While my handheld has entries for the
  various hotspots that I use, my mobile and base stations do not need
  entries for hotspots.

* On radios that have APRS capabilities (handheld and mobile) I want to have
  them programmed with the correct SSID for each radio. Yet my base station
  at home does not need to have APRS turned on since it does not move--at
  least until a major earthquake hits and my house slides down the mountain.

* Most of my normal everyday radios are DMR capable, but I would like to
  have as much of the code plug as possible on several of my other radios
  that are FM only or DSTAR capable.

* On the DSTAR radios, I would like to have the DSTAR channels coordinated
  among those radios.

These are the primary reasons, but there are probably a couple more that I
am not thinking of at the moment.

